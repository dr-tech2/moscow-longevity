<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AttendsController;
use App\Http\Controllers\Api\QuestionsController;
use App\Http\Controllers\Api\SurveyController;
use App\Http\Controllers\Api\AnswerController;
use App\Http\Controllers\Api\ClientController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    ['prefix' => '/v1', 'namespace' => 'Api'],
    function () {
        Route::get('/client', [ClientController::class, 'getClient']);

        Route::get('/activities', [AttendsController::class, 'getActivities']);
        Route::get('/activity', [AttendsController::class, 'getActivity']);

        Route::get('/recommends', [AttendsController::class, 'getRecommends']);
        Route::get('/questions', [QuestionsController::class, 'getQuestions']);

        Route::get('/groups-by-activities', [AttendsController::class, 'getGroupsByActivityIds']);

        Route::get('/survey', [SurveyController::class, 'getSurvey']);
        Route::post('/answer', [AnswerController::class, 'storeAnswer']);
        Route::get('/possible-answers', [AnswerController::class, 'getPossibleAnswers']);
        Route::post('/survey', [SurveyController::class, 'createSurvey']);
        Route::post('/survey/update', [SurveyController::class, 'updateSurvey']);

        Route::group(['prefix' => '/raw'], function() {
           Route::get('/activities', [AttendsController::class, 'getRawActivities']);
           Route::get('/groups', [AttendsController::class, 'getRawGroups']);
        });
    }
);
