<?php

namespace Database\Seeders;

use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::truncate();

        $questions = [
            [
                'id' => 1,
                'type' => 'text',
                'text' => 'Введите Вашу Фамилию Имя и Отчество',
                'description' => null,
                'placeholder' => 'Фамилия Имя Отчетво',
                'answers' => null,
            ],
            [
                'id' => 2,
                'type' => 'date',
                'text' => 'Введите Вашу дату рождения в формате год, месяц, день.',
                'description' => null,
                'placeholder' => 'Дата рождения',
                'answers' => null,
            ],
            [
                'id' => 3,
                'type' => 'address',
                'text' => 'Укажите Ваш адрес проживания',
                'description' => null,
                'placeholder' => null,
                'answers' => null,
            ],
            [
                'id' => 4,
                'type' => 'radio',
                'text' => 'Есть ли у Вас заболевания, ограничивающие физическую активность?',
                'description' => null,
                'placeholder' => null,
                'answers' => ['Да', 'Нет'],
            ],
            [
                'id' => 5,
                'type' => 'radio',
                'text' => 'Как Вам было бы удобно заниматься?',
                'description' => null,
                'placeholder' => null,
                'answers' => ['Онлайн (через интернет)', 'Оффлайн (очное занятие)', 'Оба варианта'],
            ],
            [
                'id' => 6,
                'type' => 'checkbox',
                'text' => 'В какое время Вам будет удобно заниматься?',
                'description' => null,
                'placeholder' => null,
                'answers' => [
                    'В промежуток с 8:00 до 10:00',
                    'В промежуток с 11:00 до 13:00',
                    'В промежуток с 14:00 до 16:00',
                    'В промежуток с 17:00 до 19:00',
                ],
            ],
            [
                'id' => 7,
                'type' => 'checkbox-images',
                'text' => 'Какие программы были бы самыми полезными для развития Ваших навыков и нераскрытого потенциала?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 8,
                'type' => 'checkbox-images',
                'text' => 'Какое из следующих занятий вызывает у Вас наибольший интерес?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 9,
                'type' => 'checkbox-images',
                'text' => 'Какие программы, по Вашему мнению, способны привнести больше радости и счастья в Вашу жизнь?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 10,
                'type' => 'checkbox-images',
                'text' => 'Какие виды деятельности Вам всегда привлекали или вызывали интерес?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
            [
                'id' => 11,
                'type' => 'checkbox-images',
                'text' => 'Какие программы занятий вызывают у Вас наибольшую страсть или восторг?',
                'description' => null,
                'placeholder' => null,
                'load_answers' => true,
                'answers' => null,
            ],
        ];

        foreach ($questions as $question) {
            Question::create($question);
        }
    }
}
