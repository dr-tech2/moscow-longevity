<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\GenderActivity;

class GenderActivitiesSeeder extends Seeder
{
    /** @var string  */
    protected string $dir;

    public function __construct()
    {
        $this->dir = database_path() . '/seeders/data';
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GenderActivity::truncate();

        $this->loadGenderActivities('Муж.csv', 'Мужской');
        $this->loadGenderActivities('Жен.csv', 'Женский');
    }

    protected function loadGenderActivities(string $filename, string $gender): void
    {
        $handle = fopen($this->dir . '/' . $filename, "r");
        $columns = ['n','age_group','dict_column1'];
        $ageGroupPattern = "/\((-?\d+(\.\d+)?),\s*(-?\d+(\.\d+)?)\]/ius";
        $matches = [];
        $counter = 0;

        while (($data = fgetcsv($handle, null, ',')) !== false) {
            if ($counter++ == 0) {
                continue;
            }

            $combined = array_combine($columns, $data);
            preg_match($ageGroupPattern, $combined['age_group'], $matches);
            $ids = json_decode($combined['dict_column1'], true);
            foreach ($ids as $id) {
                GenderActivity::create([
                    'id_level3' => $id,
                    'gender' => $gender,
                    'age_min' => (int)($matches[1] - 1),
                    'age_max' => (int)$matches[3],
                ]);
            }
        }
    }
}
