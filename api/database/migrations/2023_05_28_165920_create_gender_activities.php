<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gender_activities', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer('id_level3')->nullable()->index();
            $table->string('gender')->nullable();
            $table->integer('age_min')->nullable();
            $table->integer('age_max')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gender_activities');
    }
};
