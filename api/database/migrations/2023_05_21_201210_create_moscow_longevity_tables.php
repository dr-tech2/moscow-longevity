<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //уникальный номер,дата создание личного дела,пол,дата рождения,адрес проживания
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->string('gender')->nullable();
            $table->date('birthdate')->nullable();
            $table->text('address')->nullable();
            $table->string('fio', 512)->nullable();
        });

        //уникальный номер,
        //направление 1,
        //направление 2,
        //направление 3,
        //адрес площадки,
        //округ площадки,
        //район площадки,
        //расписание в активных периодах,
        //расписание в закрытых периодах,
        //расписание в плановом периоде
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->integer('activity_id')->nullable()->index();
            $table->string('direction_1')->nullable();
            $table->string('direction_2')->nullable();
            $table->string('direction_3')->nullable();
            $table->text('address')->nullable();
            $table->text('district')->nullable();
            $table->text('area')->nullable();
            $table->text('schedule_active_periods')->nullable();
            $table->text('schedule_close_periods')->nullable();
            $table->text('schedule_plan_period')->nullable();
        });

        //уникальный номер занятия,
        //уникальный номер группы,
        //уникальный номер участника,
        //направление 2,
        //направление 3,
        //онлайн/офлайн,
        //дата занятия,
        //время начала занятия,
        //время окончания занятия
        Schema::create('attends', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->integer('group_id')->nullable()->index();
            $table->integer('client_id')->nullable()->index();
            $table->string('direction_1')->nullable();
            $table->string('direction_2')->nullable();
            $table->boolean('online')->default(false);
            $table->date('attend_date')->nullable();
            $table->time('attend_start_time')->nullable();
            $table->time('attend_end_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('attends');
    }
};
