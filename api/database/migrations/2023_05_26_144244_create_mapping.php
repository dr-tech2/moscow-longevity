<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mappings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('code')->nullable()->index();
            $table->string('name')->nullable();
            $table->string('type')->nullable()->index();
            $table->text('description')->nullable();
        });

        Schema::table('activities', function (Blueprint $table) {
            $table->integer('mapping_id')->nullable()->index();
            $table->string('answer')->nullable();
            $table->boolean('physical_limitations')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mappings');
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn(['mapping_id', 'answer', 'mapping_id']);
        });
    }
};
