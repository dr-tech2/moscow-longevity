<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('type')->nullable();
            $table->text('text')->nullable();
            $table->text('description')->nullable();
            $table->string('placeholder')->nullable();
            $table->boolean('load_answers')->default(false);
            $table->jsonb('answers')->nullable();
        });

        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer('client_id')->nullable()->index();
            $table->integer('status_id')->nullable()->index();
        });

        Schema::create('answers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer('survey_id')->nullable()->index();
            $table->integer('question_id')->nullable()->index();
            $table->text('question')->nullable();
            $table->jsonb('possible_answers')->nullable();
            $table->string('answer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
        Schema::dropIfExists('surveys');
        Schema::dropIfExists('answers');
    }
};
