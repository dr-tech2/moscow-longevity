<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\SurveyService;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class SurveyController
 *
 * @package App\Http\Controllers\Api
 */
class SurveyController extends Controller
{
    /**
     * @param Request $request
     * @param SurveyService $surveyService
     *
     * @return JsonResponse
     */
    public function createSurvey(Request $request, SurveyService $surveyService): JsonResponse
    {
        $params = $request->all();

        try {
            $result = $surveyService->createSurvey($params['client_id'] ?? null);
        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @param SurveyService $surveyService
     *
     * @return JsonResponse
     */
    public function updateSurvey(Request $request, SurveyService $surveyService): JsonResponse
    {
        $request->validate([
            'survey_id' => ['required', 'integer'],
        ]);

        $params = $request->all();

        try {
            $result = $surveyService->updateSurvey($params['survey_id'], array_diff_key($params, ['survey_id' => 1]));
        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @param SurveyService $surveyService
     *
     * @return JsonResponse
     */
    public function getSurvey(Request $request, SurveyService $surveyService): JsonResponse
    {
        $request->validate([
            'survey_id' => ['required', 'integer'],
        ]);

        $params = $request->all();

        try {
            $result = $surveyService->getSurvey($params['survey_id']);
        } catch (\Throwable $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json($result);
    }
}
