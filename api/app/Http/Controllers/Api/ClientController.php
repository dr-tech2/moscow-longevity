<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\ClientService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ClientController
 *
 * @package App\Http\Controllers\Api
 */
class ClientController extends Controller
{
    /**
     * Возвращает данные о клиенте
     *
     * @param Request $request
     * @param ClientService $clientService
     *
     * @return JsonResponse
     */
    public function getClient(Request $request, ClientService $clientService): JsonResponse
    {
        $request->validate([
            'survey_id' => ['required', 'integer'],
        ]);

        $params = $request->all();

        try {
            $result = $clientService->getClient($params);
        } catch (\Throwable $exception) {
            $result = [];
        }

        return response()->json($result);
    }
}
