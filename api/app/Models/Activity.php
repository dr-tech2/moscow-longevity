<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Activity
 *
 * @property integer $id
 * @property string $type
 * @property integer $id_level1
 * @property string $level1
 * @property integer $id_level2
 * @property string $level2
 * @property integer $id_level3
 * @property string $level3
 * @property string $d_level1
 * @property string $d_level2
 * @property string $d_level3
 * @property boolean $online
 * @property integer $mapping_id
 * @property string $answer
 * @property boolean $physical_limitations
 *
 * @property Group[] $groups
 * @property Mapping $mapping
 *
 * @package App\Models
 */
class Activity extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @return HasMany
     */
    public function groups(): HasMany
    {
        return $this->hasMany(Group::class, 'activity_id', 'id')->where('predict_usage_flag', true);
    }

    /**
     * @return HasOne
     */
    public function mapping(): HasOne
    {
        return $this->hasOne(Mapping::class, 'id', 'mapping_id');
    }
}
