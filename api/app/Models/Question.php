<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 *
 * @property int $id
 * @property string $type
 * @property string $text
 * @property string $description
 * @property string $placeholder
 * @property boolean $load_answers
 * @property array $answers
 *
 * @package App\Models
 */
class Question extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $casts = [
        'answers' => 'array',
    ];
}
