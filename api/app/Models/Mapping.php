<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Mapping
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $type
 * @property string $description
 *
 * @package App\Models
 */
class Mapping extends Model
{
    use HasFactory;

    protected $guarded = [];

}
