<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Answer
 *
 * @property int $id
 * @property int $dialog_id
 * @property int $question_id
 * @property string $question
 * @property array $possible_answers
 * @property string $answer
 *
 * @package App\Models
 */
class Answer extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'possible_answers' => 'array',
    ];

    /**
     * @return HasOne
     */
    public function question(): HasOne
    {
        return $this->hasOne(Question::class, 'id', 'question_id');
    }
}
