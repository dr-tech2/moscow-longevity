<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Attend
 *
 * @property int $group_id
 * @property int $client_id
 * @property string $direction_1
 * @property string $direction_2
 * @property boolean $online
 * @property string $attend_date
 * @property string $attend_start_time
 * @property string $attend_end_time
 *
 * @package App\Models
 */
class Attend extends Model
{
    use HasFactory;

    protected $guarded = [];
}
