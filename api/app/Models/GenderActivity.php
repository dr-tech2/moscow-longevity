<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class GenderActivity
 *
 * @property int $id
 * @property int $id_level3
 * @property string $gender
 * @property int $age_min
 * @property int $age_max
 *
 * @property Activity $activity
 *
 * @package App\Models
 */
class GenderActivity extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @return HasOne
     */
    public function activity(): HasOne
    {
        return $this->hasOne(Activity::class, 'id_level3', 'id_level3');
    }
}
