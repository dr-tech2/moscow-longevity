<?php


namespace App\Helpers;

/**
 * Class Helper
 *
 * @package App\Helpers
 */
class Helper
{
    /**
     * @param $query
     *
     * @return string
     */
    public static function getSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }

    /**
     * @param string $schedulePeriod
     *
     * @return array
     */
    public static function convertStringPeriodsToArrayOfTimes(string $schedulePeriod): array
    {
        if (empty($schedulePeriod)) {
            return [];
        }

        $cleanString = preg_replace("/[^a-zA-Zа-яА-Я0-9,:.\s-]/u", "", $schedulePeriod);
        $weekdaysRegex = "/([а-яА-Я]{2,4}\.?)/u";
        preg_match_all($weekdaysRegex, $cleanString, $weekdayMatches);
        $weekdays = array_filter($weekdayMatches[0], function($item) {
            return in_array($item, ['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вс.']);
        });

        $timesRegex = "/(\d{1,2}:\d{1,2}-\d{1,2}:\d{1,2})/u";
        preg_match_all($timesRegex, $cleanString, $timeMatches);
        $times = $timeMatches[0];

        $result = [];
        foreach ($weekdays as $weekday) {
            $result[] = trim($weekday, '.') . ' ' . $times[0];
        }

        return $result;
    }

    /**
     * @param array $preferredTimeIntervals
     * @param string $groupTimeInterval
     *
     * @return bool
     */
    public static function isTimeIntervalContained(array $preferredTimeIntervals, string $groupTimeInterval): bool
    {
        /**
         * @param string $str
         *
         * @return array|null[]
         *
         * @throws \Exception
         */
        $parseTimes = function(string $str) {
            $pattern = '/\d{1,2}:\d{2}/';
            preg_match_all($pattern, $str, $times);
            if (count($times[0]) !== 2) {
                throw new \Exception('Incorrect time');
            }

            $startTime = strtotime($times[0][0]);
            $endTime = strtotime($times[0][1]);

            if ($startTime === false || $endTime === false) {
                throw new \Exception('Incorrect time');
            }

            return [$startTime, $endTime];
        };

        try {
            [$groupStartTime, $groupEndTime] = $parseTimes($groupTimeInterval);

            foreach ($preferredTimeIntervals as $preferredTimeInterval) {
                [$prefStartTime, $prefEndTime] = $parseTimes($preferredTimeInterval);
                if ($prefStartTime <= $groupStartTime && $prefEndTime >= $groupEndTime) {
                    return true;
                }

            }

        } catch (\Throwable $exception) {

        }

        return false;
    }

    /**
     * Переводит строку вида город москва, онежская, дом 17, корпус 4
     * в массив {street:..., house:..., building:..., construction:...}
     *
     * @param string $address
     * @return array
     */
    public static function parseAddress(string $address): array
    {
        $fields = [
            'street' => null,
            'house' => null,
            'building' => null,
            'construction' => null
        ];

        $pos = mb_stripos($address, 'москва,');
        if ($pos !== false) {
            $address = trim(mb_substr($address, $pos + mb_strlen('москва,'), mb_strlen($address)));
        }

        $parts = explode(',', $address);
        $fields['street'] = trim($parts[0]);

        foreach ($parts as $part) {
            $trimmedPart = trim($part);

            if (mb_stripos($trimmedPart, 'дом') !== false) {
                $fields['house'] = preg_replace('/\D/', '', $trimmedPart);
            } elseif (mb_stripos($trimmedPart, 'строение') !== false) {
                $fields['building'] = preg_replace('/\D/', '', $trimmedPart);
            } elseif (mb_stripos($trimmedPart, 'корпус') !== false) {
                $fields['construction'] = preg_replace('/\D/', '', $trimmedPart);
            }
        }

        return $fields;
    }
}
