<?php


namespace App\Dto;

/**
 * Class BaseDto
 *
 * @package App\DTO
 */
abstract class BaseDto
{
    /** @var array  */
    protected array $data;

    /** @var array */
    protected array $required = [];

    /** @var array  */
    protected array $fields = [];

    /**  */
    protected function validate(): void
    {
        $incor = [];
        foreach ($this->required as $key) {
            if (!isset($this->data[$key]))
                $incor[] = $key;
        }

        if (!empty($incor))
            throw new \InvalidArgumentException('Dto ' . static::class . ' has incor fields: ' . join(', ', $incor));
    }

    public function __construct(array $data)
    {
        $this->data = $data;

        $this->validate();
    }

    /**
     * @param array $fields
     */
    public function setFields(array $fields): void
    {
        foreach ($fields as $key => $field) {
            $this->data[$key] = $field;
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return !empty($this->fields) ? array_intersect_key($this->data, array_flip($this->fields)) : $this->data;
    }
}
