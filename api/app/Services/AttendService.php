<?php


namespace App\Services;

use App\Dto\RecSysPredictDto;
use App\Helpers\Helper;
use App\Models\Activity;
use App\Models\Answer;
use App\Models\Group;
use Illuminate\Support\Collection;

/**
 * Class AttendService
 *
 * @package App\Services
 */
class AttendService
{
    /** @var array  */
    protected array $activityTypesDict;

    protected array $day = [
        1 => 'Пн',
        2 => 'Вт',
        3 => 'Ср',
        4 => 'Чт',
        5 => 'Пт',
        6 => 'Сб',
        7 => 'Вс',
    ];

    protected array $time = [
        '8:00-10:00' => [
            '08:00',
            '09:00',
            '10:00',
        ],
        '11:00-13:00' => [
            '11:00',
            '12:00',
            '13:00',
        ],
        '14:00-16:00' => [
            '14:00',
            '15:00',
            '16:00',
        ],
        '17:00-19:00' => [
            '17:00',
            '18:00',
            '19:00',
        ],
    ];

    public function __construct()
    {
        $this->activityTypesDict = [
            'Для тела' => 'Физическая активность',
            'Для ума' => 'Образование',
            'Для души' => 'Творчество',
        ];
    }

    /**
     * Возвращает информацию о мероприятии
     *
     * @param int $id
     *
     * @return array
     */
    public function getActivity(int $id): array
    {
        /** @var Activity $activity */
        $activity = Activity::where('id_level3', $id)->first();
        if (empty($activity)) {
            return [];
        }

        $groups = [];
        foreach ($activity->groups as $group) {
            $times = Helper::convertStringPeriodsToArrayOfTimes($group->schedule_active_periods);
            if (empty($times)) {
                continue;
            }

            $groups[] = [
                'name' => 'G-' . $group->id,
                'status' => 'Группа занимается',
                'times' => $times,
                'address' => $group->address,
            ];
        }

        return [
            'id' => $id,
            'address' => $activity->online ? 'Онлайн' : '',
            'title' => $activity->level3,
            'description' => $activity->d_level1,
            'groups' => $groups,
        ];
    }

    /**
     * Возвращает список мероприятия по фильтру
     *
     * @param array $params
     *
     * @return array
     */
    public function getActivities(array $params): array
    {
        [
            $client,
            $interests,
            $online,
            $physicalLimitations,
            $preferredTimes,
            $type,
            $day,
            $time
        ] = [
            null,
            [],
            (bool) $params['online'],
            false,
            [],
            $params['type'],
            $params['day'],
            $params['time']
        ];

        if (!empty($params['survey_id'] ?? null)) {
            $answerService = new AnswerService();

            $answers = $answerService->getAnswers($params['survey_id']);
            $client = $answerService->getClient($answers);
            $interests = $answerService->getClientInterests($answers);
            $physicalLimitations = $answerService->getPhysicalLimitations($answers);
            $online = $answerService->getOnline($answers);
            $preferredTimes = $answerService->getPreferredTimeIntervals($answers);
        }

        //получаем список всех групп в порядке релевантности
        $activitiesIdsSorted = (new RecSysService())->predictActivities(new RecSysPredictDto([
            'user_id' => !empty($client) ? $client->id : 0,
            'n_top' => 900,
        ]));

        $this->sortPredictedWithInterests($interests, $activitiesIdsSorted);

        $activities = Activity::whereIn('id_level3', $activitiesIdsSorted['activities_id'])
            ->when(is_bool($online), function($query) use ($online) {
                return $query->where('online', $online);
            })
            ->when($type, function($query) use ($type) {
                return $query->where('type', $type);
            })
            ->whereRaw('level1 not like \'%спецпроект%\'')
            ->when($physicalLimitations, function ($query) {
                return $query->where('physical_limitations', false);
            })
            ->when($str = ($params['query'] ?? null), function($query) use ($str) {
                return $query->whereRaw('d_level1 || \' \' || level2 || \' \' || level3 like \'%' . trim($str) . '%\'');
            })
            ->whereHas('groups', function($query) use ($day, $time) {
                return $query->when($day, function ($query) use ($day) {
                            return $query->whereRaw('schedule_active_periods like \'%' . $this->day[$day] . '%\'');
                       })
                       ->when($time, function ($query) use ($time) {
                            return $query->whereRaw('(schedule_active_periods like \'%' . $this->time[$time][0] . '%\'')
                                    ->orWhereRaw('schedule_active_periods like \'%' . $this->time[$time][1] . '%\'')
                                    ->orWhereRaw('schedule_active_periods like \'%' . $this->time[$time][2] . '%\')');
                       })
                       ->whereRaw('LENGTH(schedule_active_periods) > 0');
            })
            ->whereHas('groups', function($query) {
                return $query->whereRaw('LENGTH(schedule_active_periods) > 0');
            })
            ->get()
            ->keyBy('id_level3');

        $count = count($activities);
        $result = [];
        $types = [];
        $counter = 0;
        $offset = $params['offset'] ?? 0;
        foreach ($activitiesIdsSorted['activities_id'] as $item) {
            if (!isset($activities[$item])) {
                continue;
            }

            if ($counter >= ($params['limit'] + $offset)) {
                continue;
            }

            /** @var Activity $activity */
            $activity = $activities[$item];
            $types[] = $activity->type;
            if ($counter >= ($params['limit'] + $offset - 1) && count($types) < 3) {
                continue;
            }

            $filteredGroups = $this->getFilteredGroups(collect($activity->groups), $preferredTimes, $time);
            if (empty($filteredGroups)) {
                continue;
            }

            $result[$activity->id] = [
                'id' => $activity->id_level3,
                'type' => $activity->type,
                'title' => $activity->level3,
                'description' => $activity->d_level1,
                'groups' => [],
            ];

            foreach ($filteredGroups as $group) {
                if (count($result[$activity->id]['groups']) >= 3) {
                    continue;
                }

                $result[$activity->id]['groups'][] = [
                    'status' => 'Группа занимается',
                    'name' => 'G-' . $group['id'],
                    'times' => $group['times'],
                    'address' => $group['address'],
                ];
            }

            $counter += 1;
        }

        return [
            'items' => array_slice(array_values($result), $offset, $params['limit']),
            'total' => $count,
        ];
    }

    /**
     * Возвращает список рекомендованных к посещению мероприятий
     *
     * @param array $params
     *
     * @return array
     */
    public function getRecs(array $params): array
    {
        $recs = (new RecSysService())->predictActivities(new RecSysPredictDto([
            'user_id' => $params['user_id'] ?? 0, 'n_top' => 900
        ]));

        $activities = Activity::whereIn('id_level3', $recs['activities_id'])->get();
        $result = [];

        /** @var Activity $activity */
        foreach ($activities as $activity) {
            if (!isset($result[$activity->type])) {
                $result[$activity->type] = [
                    'title' => $this->activityTypesDict[$activity->type] ?? $activity->type,
                    'items' => []
                ];
            }

            if (count($result[$activity->type]['items']) >= 3) {
                continue;
            }

            $result[$activity->type]['items'][] = [
                'id' => $activity->id_level3,
                'name' => $activity->level3,
            ];
        }

        return array_values($result);
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getRawActivities(array $params): array
    {
        $result = [];
        $recs = (new RecSysService())->predictActivities(new RecSysPredictDto($params));
        $activities = Activity::whereIn('id_level3', $recs['activities_id'])
            ->get()
            ->keyBy('id_level3');

        foreach ($recs['activities_id'] as $id) {
            /** @var Activity $activity */
            $activity = $activities[$id];

            $result[] = [
                'id_level3' => $id,
                'type' => $activity->type,
                'level1' => $activity->level1,
                'level2' => $activity->level2,
                'level3' => $activity->level3,
                'd_level1' => $activity->d_level1,
            ];
        }

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getRawGroups(array $params): array
    {
        $result = [];
        $groupList = (new RecSysService())->predictGroups(new RecSysPredictDto($params));
        $groups = Group::whereIn('id', $groupList['groups_id'])
            ->where('predict_usage_flag', true)
            ->get()
            ->keyBy('id');

        foreach ($groupList['groups_id'] as $id) {
            if (!isset($groups[$id])) {
                continue;
            }

            /** @var Group $group */
            $group = $groups[$id];

            $result[] = [
                'id' => $id,
                'direction_1' => $group->direction_1,
                'direction_2' => $group->direction_2,
                'direction_3' => $group->direction_3,
                'address' => $group->address,
                'schedule_active_periods' => $group->schedule_active_periods,
            ];
        }

        return $result;
    }

    /**
     * @param array $interests
     * @param array $predicted
     */
    private function sortPredictedWithInterests(array $interests, array &$predicted): void
    {
        if (empty($interests)) {
            return;
        }

        $interestedActivities = Activity::whereIn('answer', $interests)->get(['id_level3'])->pluck('id_level3')->toArray();
        $predicted['activities_id'] = array_unique(array_merge($interestedActivities, $predicted['activities_id']));
    }

    /**
     * Сортирует группы по релевантности
     *
     * @param Collection $groups
     * @param array $preferredTimeIntervals
     *
     * @return array
     */
    private function getFilteredGroups(Collection $groups, array $preferredTimeIntervals, $timeFilter): array
    {
        $result = [];
        /** @var Group $group */
        foreach ($groups as $group) {
            $times = Helper::convertStringPeriodsToArrayOfTimes($group->schedule_active_periods);

            if (empty($times)) {
                continue;
            }

            if (!empty($preferredTimeIntervals)) {
                if (!Helper::isTimeIntervalContained($preferredTimeIntervals, $group->schedule_active_periods)) {
                    continue;
                }
            }

            $result[] = [
                'id' => $group->id,
                'times' => $times,
                'address' => $group->address,
            ];
        }

        return $result;
    }
}
