<?php


namespace App\Services;

use App\Helpers\Helper;

/**
 * Class ClientService
 *
 * @package App\Services
 */
class ClientService
{
    /**
     * @param array $params
     *
     * @return array
     */
    public function getClient(array $params): array
    {
        $answerService = new AnswerService();
        $answers = $answerService->getAnswers($params['survey_id']);

        $client = $answerService->getClient($answers);
        //не удалось авторизовать клиента
        if (empty($client)) {
            return [];
        }

        $parsedAddress = Helper::parseAddress($client->address);

        return [
            'id' => $client->id,
            'gender' => $client->gender,
            'address' => $parsedAddress,
            'address_raw' => $client->address,
        ];
    }
}
