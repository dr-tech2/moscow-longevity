<?php


namespace App\Services;

use App\Models\Survey;

/**
 * Class SurveyService
 *
 * @package App\Services
 */
class SurveyService
{
    /**
     * @param int $surveyId
     *
     * @return array
     */
    public function getSurvey(int $surveyId): array
    {
        $answers = [];
        /** @var Survey $survey */
        $survey = Survey::find($surveyId);
        foreach ($survey->answers as $answer) {
            $json = json_decode($answer->answer, true);
            $answers[] = [
                'question_id' => $answer->question_id,
                'answer' => is_object($json) ? $json : $answer->answer,
                'possible_answers' => $answer->possible_answers,
            ];
        }

        return [
            'id' => $surveyId,
            'created_at' => $survey->created_at,
            'answers' => $answers,
        ];
    }

    /**
     * @param int|null $clientId
     *
     * @return array
     */
    public function createSurvey(?int $clientId): array
    {
        /** @var Survey $survey */
        $survey = Survey::create([
            'client_id' => $clientId,
            'status_id' => 1,
        ]);

        return [
            'id' => $survey->id,
            'created_at' => $survey->created_at,
            'answers' => [],
        ];
    }

    /**
     * @return bool[]
     */
    public function updateSurvey(int $surveyId, array $params): array
    {
        Survey::find($surveyId)->update($params);

        return ['success' => true];
    }
}
