<?php

namespace App\Services;

use App\Dto\RecSysPredictDto;
use App\Helpers\Helper;
use App\Models\Activity;
use App\Models\Group;
use Illuminate\Support\Facades\DB;

/**
 * Class GroupService
 *
 * @package App\Services
 */
class GroupService
{
    /**
     * Возращает группы по активностям на основе расстояния между группой и пользователяем
     *
     * @param int $userId
     * @param array $activityIds
     * @param int $limitPerActivity
     *
     * @return array
     */
    public function getGroupsByActivities(int $userId, array $activityIds, int $limitPerActivity = 10): array
    {
        if (empty($activityIds)) {
            return [];
        }

        $recsysService = new RecSysService();
        $result = [];
        //получаем список групп по активностям
        $builder = Group::orderBy('id')
            ->whereRaw('direction_1 not like \'Спецпроект%\'')
            ->where(DB::raw('LENGTH(schedule_active_periods)'), '>', 0)
            ->where('predict_usage_flag', true)
            ->whereHas('activity', function($query) use ($activityIds) {
                return $query->whereIn('id_level3', $activityIds)->where('online', false);
            });

        $groups = $builder->get();

        /** @var Group $group */
        foreach ($groups as $group) {
            if (count($result[$group->activity_id] ?? []) >= $limitPerActivity) {
                continue;
            }

            $res = $recsysService->getUserGroupDistance(new RecSysPredictDto([
                'user_id' => $userId,
                'group_id' => $group->id,
            ]));

            if (empty($res['distance'])) {
                continue;
            }

            $result[$group->activity_id] = [
                'distance' => $res['distance'],
                'status' => 'Группа занимается',
                'name' => 'G-' . $group->id,
                'times' => Helper::convertStringPeriodsToArrayOfTimes($group->schedule_active_periods),
            ];
        }

        return array_values($result);
    }
}
