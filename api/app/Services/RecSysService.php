<?php


namespace App\Services;

use App\Dto\RecSysPredictDto;
use GuzzleHttp\Client;
use \Illuminate\Http\Response;

/**
 * Class RecSysService
 *
 * @package App\Services
 */
class RecSysService
{
    /** @var Client  */
    protected Client $client;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        if (empty(env('RECSYS_API_URL'))) {
            throw new \Exception('RECSYS_API_URL is empty');
        }

        $this->client = new Client([
            'timeout' => 60.0,
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'base_uri' => env('RECSYS_API_URL'),
        ]);
    }

    /**
     * @param RecSysPredictDto $recSysPredictDto
     *
     * @return array
     */
    public function predictActivities(RecSysPredictDto $recSysPredictDto): array
    {
        $result = [];

        try {
            $response = $this->client->get('/predict_activities', [
                'json' => $recSysPredictDto->toArray(),
            ]);

            if ($response->getStatusCode() === Response::HTTP_OK) {
                $result = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
            }
        } catch (\Throwable $exception) {
            $result['error'] = $exception->getMessage();
        }

        return $result;
    }

    /**
     * @param RecSysPredictDto $recSysPredictDto
     *
     * @return array
     */
    public function predictGroups(RecSysPredictDto $recSysPredictDto): array
    {
        $result = [];

        try {
            $response = $this->client->get('/predict_groups', [
                'json' => $recSysPredictDto->toArray(),
            ]);

            if ($response->getStatusCode() === Response::HTTP_OK) {
                $result = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
            }

        } catch (\Throwable $exception) {
            $result['error'] = $exception->getMessage();
        }

        return $result;
    }

    /**
     * Возвращает расстояние между адресами пользователя и группы
     *
     * @param RecSysPredictDto $recSysPredictDto
     *
     * @return array
     */
    public function getUserGroupDistance(RecSysPredictDto $recSysPredictDto): array
    {
        $result = [];

        try {
            $response = $this->client->get('/user_group_distance', [
                'json' => $recSysPredictDto->toArray(),
            ]);

            if ($response->getStatusCode() === Response::HTTP_OK) {
                $result = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
            }

        } catch (\Throwable $exception) {
            $result['error'] = $exception->getMessage();
        }

        return $result;
    }
}
