<?php


namespace App\Services;

use App\Dto\RecSysPredictDto;
use App\Helpers\Helper;
use App\Models\Answer;
use App\Models\Client;
use App\Models\GenderActivity;
use App\Models\Question;
use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class AnswerService
 *
 * @package App\Services
 */
class AnswerService
{
    protected const NO_ONE_ANSWER = 'Затрудняюсь ответить';
    protected const QUESTION_ID_FIO = 1;
    protected const QUESTION_ID_BIRTHDATE = 2;
    protected const QUESTION_ID_ADDRESS = 3;
    protected const QUESTION_ID_PHYSICAL_LIMITATIONS = 4;
    protected const QUESTION_ID_ONLINE = 5;
    protected const QUESTION_ID_TIME_INTERVALS = 6;
    protected const MIN_AGE = 50;

    /**
     * @param int $surveyId
     *
     * @return Collection
     */
    public function getAnswers(int $surveyId): Collection
    {
        return collect(Answer::where('survey_id', $surveyId)->get()->keyBy('question_id'));
    }

    /**
     * Должно быть 4 ответа для групп: души, тела, ума и доп (спецпроект или здоровье)
     * Для каждой группы (души, тела, ума и доп)
     * нужно брать топ варианты из предсказаний Сергея и выдавать в виде ответа значение из столбца “Ответы” таблицы
     * Каждый вариант ответа из столбца “Ответ” может выдаваться только для одного вопроса. Если выпадет вариант ответа,
     * который уже был ранее, нужно брать следующее значение из списка предсказаний Сергея для соответствующей группы.
     *
     * @param array $params
     *
     * @return array
     */
    public function getPossibleAnswers(array $params): array
    {
        // getActivises
        // Проверить авторизацию и проверить наличие активностей у пользователя

//        $activitiesIdsSorted = (new RecSysService())->predictActivities(new RecSysPredictDto([
//            'user_id' => !empty($client) ? $client->id : 0,
//            'n_top' => 900,
//        ]));

        $answers = $this->getAnswers($params['survey_id']);

        try {
            $birthdate = Carbon::parse(trim($answers[self::QUESTION_ID_BIRTHDATE]->answer, "\""));
            $age = $birthdate->age;
        } catch (\Throwable $exception) {
            $age = null;
        }

        $physicalLimitations = $this->getPhysicalLimitations($answers);
        $online = $this->getOnline($answers);
        $client = $this->getClient($answers);
        $usedVariants = [];
        /** @var Answer $answer */
        foreach ($answers as $answer) {
            if ($answer->question_id > self::QUESTION_ID_TIME_INTERVALS && !empty($answer->possible_answers)) {
                $usedVariants = array_merge($usedVariants, $answer->possible_answers);
            }
        }

        //получаем список всех астивностей в порядке релевантности с привязвкой к полу, возрасту и ранее отвеченным ответам
        $builder = GenderActivity::orderBy('id')
            ->when(!empty($client), function($query) use ($client) {
                $gender = mb_stripos($client->gender, 'Жен') !== false ? 'Женский' : 'Мужской';
                return $query->where('gender', $gender);
            })
            ->when(!empty($age) && $age >= self::MIN_AGE, function($query) use ($age) {
                return $query->whereRaw($age . ' >= age_min and ' . $age . ' <= age_max');
            })
            ->whereHas('activity', function($query) use ($online, $physicalLimitations, $usedVariants) {
                $query->when(is_bool($online), function($sub) use ($online) {
                    return $sub->where('online', $online);
                });

                $query->when($physicalLimitations, function($sub) {
                    return $sub->where('physical_limitations', false);
                });

                return $query->whereNotIn('answer', $usedVariants);
            });

        $variants = [];
        $genderActivities = $builder->get();
        $additionalActivities = [];
        /** @var GenderActivity $genderActivity */
        foreach ($genderActivities as $genderActivity) {
            if (isset($variants[$genderActivity->activity->type])) {
                if (!isset($variants[$genderActivity->activity->type][$genderActivity->activity->answer])) {
                    $additionalActivities[] = [
                        'answer' => $genderActivity->activity->answer,
                        'description' => $genderActivity->activity->mapping->description,
                    ];
                }

                continue;
            }

            $variants[$genderActivity->activity->type] = [
                'answer' => $genderActivity->activity->answer,
                'description' => $genderActivity->activity->mapping->description,
            ];
        }

        $values = array_values($variants);
        while(count($values) < 3) {
            foreach ($additionalActivities as $item) {
                $true = true;

                foreach ($values as $value) {
                    if ($value['answer'] == $item['answer']) {
                        $true = false;
                    }
                }

                if ($true) {
                    $values[] = $item;
                    break;
                }
            }
        }



        $values[] = ['answer' => self::NO_ONE_ANSWER, 'description' => 'Мне не подходит ни одно из этих занятий'];

        return $values;
    }

    /**
     * Сохранение ответа пользователя в БД
     *
     * @param array $params
     *
     * @return array
     */
    public function storeAnswer(array $params): array
    {
        /** @var Question $question */
        $question = Question::find($params['question_id']);

        Answer::updateOrCreate([
            'survey_id' => $params['survey_id'],
            'question_id' => $params['question_id'],
        ], [
            'answer' => trim($params['answer'] ?? null, '"'),
            'possible_answers' => $params['possible_answers'] ?? null,
            'question' => $question->text,
        ]);

        return ['success' => true];
    }

    /**
     * @param Collection $answers
     *
     * @return Client|null
     */
    public function getClient(Collection $answers): ?Client
    {
        $client = null;
        if (!empty($answers)) {
            $fio = trim($answers[self::QUESTION_ID_FIO]->answer ?? '', '"');
            if (is_numeric($fio)) {
                /** @var Client $client */
                $client = Client::orderBy('id')
                    ->where('id', $fio)
                    ->first();

            } else {
                try {
                    $birthdate = Carbon::parse(trim($answers[self::QUESTION_ID_BIRTHDATE]->answer ?? '', '"'))->format('Y-m-d');
                } catch (\Throwable $exception) {
                    $birthdate = null;
                }

                if (!empty($fio) && !empty($birthdate)) {
                    /** @var Client $client */
                    $client = Client::orderBy('id')
                        ->where('birthdate', $birthdate)
                        ->where('fio', $fio)
                        ->first();
                }

            }
        }

        return $client;
    }

    /**
     * @param Collection $answers
     *
     * @return array
     */
    public function getClientInterests(Collection $answers): array
    {
        $interests = [];
        /** @var Answer $answer */
        foreach ($answers as $answer) {
            if ($answer->question_id <= 6) {
                continue;
            }

            $decoded = json_decode($answer->answer, true);
            if (empty($decoded) || !is_array($decoded)) {
                continue;
            }

            $interests = array_merge($interests, $decoded);
        }

        return $interests;
    }

    /**
     * @param Collection $answers
     *
     * @return bool|null
     */
    public function getOnline(Collection $answers): ?bool
    {
        if (!empty($answers[self::QUESTION_ID_ONLINE]->answer ?? null)) {
            $answer = trim($answers[self::QUESTION_ID_ONLINE]->answer, "\"");
            if (mb_stripos($answer, 'Онлайн') !== false) {
                return true;
            } elseif (mb_stripos($answer, 'Оффлайн') !== false) {
                return false;
            }
        }

        return null;
    }

    /**
     * @param Collection $answers
     *
     * @return bool|null
     */
    public function getPhysicalLimitations(Collection $answers): ?bool
    {
        return trim($answers[self::QUESTION_ID_PHYSICAL_LIMITATIONS]->answer ?? null, "\"") === 'Да';
    }

    /**
     * @param Collection $answers
     *
     * @return array
     */
    public function getPreferredTimeIntervals(Collection $answers): array
    {
        if (!isset($answers[self::QUESTION_ID_TIME_INTERVALS])) {
            return [];
        }

        //["В промежуток с 8:00 до 10:00","В промежуток с 11:00 до 13:00"]
        $res = json_decode($answers[self::QUESTION_ID_TIME_INTERVALS]->answer);
        if (in_array(self::NO_ONE_ANSWER, $res ?: [])) {
            return [];
        }

        return $res ?: [];
    }
}
