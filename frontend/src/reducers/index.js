import {combineReducers} from 'redux'
import {main} from "./main"
import {connectRouter} from "connected-react-router";
import {recommends} from "./recommends";

const reducers = {main: main, recommends: recommends}

const rootReducer = (history) => combineReducers({
    router: connectRouter(history),
    ...reducers
})

export default rootReducer
