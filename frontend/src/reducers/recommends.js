import {GET_RECOMMENDS_SUCCESS} from "../actions/service";

export const initialState = {
    items: [],
}

export const recommends = (state = initialState, action) => {
    switch (action.type) {
        case GET_RECOMMENDS_SUCCESS:
            return {
                ...state,
                items: action.data,
            }
        default:
            return state;
    }
}