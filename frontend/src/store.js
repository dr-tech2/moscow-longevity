import {createBrowserHistory}                                  from 'history'
import { createStore, applyMiddleware, compose }               from 'redux'
import { routerMiddleware }                                    from 'react-router-redux'
import Thunk                                                   from 'redux-thunk'
import rootReducer                                             from './reducers'

export const history = createBrowserHistory()

const middleware = [
    routerMiddleware(history),
    Thunk
]

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const enhancer = composeEnhancers(applyMiddleware(...middleware))

export const store = createStore(
    rootReducer(history),
    {},
    enhancer
)
