import React, {Component} from "react";
import styled from "styled-components";
import {Container} from "../components/atoms/container";
import {Text} from "../components/atoms/text";
import {push} from "connected-react-router";
import {connect} from "react-redux";
import {getActivity} from "../actions/service";
import {getURLSearchParams} from "../helpers/url-params-helper";
import {LinkText} from "../components/atoms/link-text";

const StyledContainer = styled(Container)`
    padding-left: 6.4375rem;
    padding-right: 6.4375rem;
    padding-bottom: 5rem;
    padding-top: 1.75rem;
`

const Title = styled(Text)`
    margin-top: 6px;
    font-weight: 600;
    font-size: 26px;
    line-height: 32px;
`

const SubTitle = styled(Text)`
    font-size: 16px;
    line-height: 24px;
    color: #696c71;
`

const Description = styled(Text)`
    line-height: 1.4;
    margin-top: 12px;
`

const GroupList = styled(Container)`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: flex-start;
    margin-top: 24px;
    width: 100%;
`

const GroupItem = styled(Container)`
    width: 284px;
    margin-right: 24px;
    padding: 16px;
    border-bottom: 1px Solid #dfe1e6;
`

const GroupStatus = styled(Text)`
    color: #084;
    margin-bottom: 8px;
    font-weight: 500;
    white-space: nowrap;
`

const GroupId = styled(Text)`
    font-weight: 500;
    font-size: 18px;
    line-height: 22px;
    margin-bottom: 8px;
    width: auto;
    margin-right: 4px;
`

const GroupIdNumber = styled(GroupId)`
    color: #717479;
    white-space: nowrap;
`

class ActivityComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {activity: null};
    }

    async componentDidMount() {
        const {id} = getURLSearchParams();
        const activity = await this.props.getActivity({id: id})

        this.setState({
            activity: activity,
        })
    }

    render() {
        const {activity} = this.state;

        return <StyledContainer flow={'column'}>
            <Container>
                <LinkText onClick={() => this.props.goBack()}>&#8592; Назад</LinkText>
            </Container>
            {activity && <Container flow={'column'}  margin={'24px 0 0 0'}>
                {activity.address && <SubTitle>{activity.address}</SubTitle>}
                <Title>{activity.title}</Title>
                <Description>{activity.description}</Description>
            </Container>}
            {activity && <GroupList>
                {activity.groups.map((group, i) => <GroupItem flow={'column'} key={'list-item_' + i}>
                    <GroupStatus>{group.status}</GroupStatus>
                    <Container flow={'row'} justify={'flex-start'}>
                        <GroupId>Группа</GroupId>
                        <GroupIdNumber>{group.name}</GroupIdNumber>
                    </Container>
                    <Container flow={'column'}>
                        {group.times && group.times.map((tm, j) => <Container
                            key={'list-item_' + i + '_tm_' + j}>
                            <Text>{tm}</Text>
                        </Container>)}
                    </Container>
                    <Container margin={'16px 0 16px 0'}>
                        {group.address && <SubTitle>{group.address}</SubTitle>}
                    </Container>
                </GroupItem>)}
                {activity.groups.length === 0 && <Text>К сожалению, активных групп сейчас нет</Text>}
            </GroupList>}
        </StyledContainer>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
    }
}

const mapDispatchToProps = {
    push,
    getActivity,
}

export const Activity = connect(mapStateToProps, mapDispatchToProps)(ActivityComponent);