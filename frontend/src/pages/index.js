import React, {Component} from "react";
import {push} from "connected-react-router";
import styled from "styled-components";
import {connect} from "react-redux";
import {Container} from "../components/atoms/container";
import Header from "../components/header";
import {List} from "./list";
import {Questions} from "./questions";
import {Recommends} from "./recommends";
import {Activity} from "./activity";
import {ContentLoader} from "../components/atoms/content-loader";
import {BaseTemplate} from "../components/templates/base";

const StyledContainer = styled(Container)`

`

class MainComponentClass extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: this.props.page,
            history: ['/'],
        };
    }

    componentDidMount() {

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {page} = this.props;
        if (prevProps.page !== page) {
            this.setState({
                page: page,
            })
        }
    }

    getContent = () => {
        const {page} = this.state;
        const props = {
            changeLocation: this.changeLocation,
            goBack: this.goBack,
        };

        switch (page && page.split('?')[0]) {
            case '/questions':
                return <Questions {...props}/>
            case '/list':
                return <List {...props}/>
            case '/activity':
                return <Activity {...props}/>
            default:
                return <Recommends {...props}/>
        }
    }

    changeLocation = (loc, store = false) => {
        const {page, history} = this.state

        store && history.push(page)

        this.props.push(loc)
        this.setState({
            page: loc,
            history: history,
        })
    }

    goBack = () => {
        const {history} = this.state

        const prevPage = history.pop()
        this.props.push(prevPage)

        this.setState({
            page: prevPage,
            history: history
        })

    }

    render() {
        return <Container flow={'column'}>
            <StyledContainer flow={'column'}>
                {this.getContent()}
            </StyledContainer>
        </Container>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
        isLoading: state.main.isLoading,
    }
}

const mapDispatchToProps = {
    push,
}

export const MainComponent = connect(mapStateToProps, mapDispatchToProps)(MainComponentClass);