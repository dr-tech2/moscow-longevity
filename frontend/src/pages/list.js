import React, {Component} from "react";
import styled from "styled-components";
import {Container} from "../components/atoms/container";
import {Text} from "../components/atoms/text";
import {BlueButton, Button} from "../components/atoms/button";
import {LinkText} from "../components/atoms/link-text";
import {push} from "connected-react-router";
import {connect} from "react-redux";
import {Input} from "../components/atoms/input";
import {Filter} from "../components/organisms/filter";
import {getActivities} from "../actions/service";
import {getURLSearchParams} from "../helpers/url-params-helper";

const StyledContainer = styled(Container)`
    background-color: #F3F5F7;
    min-height: 230px;
    padding-left: 6.4375rem;
    padding-right: 6.4375rem;
`

const ItemsContainer = styled(Container)`
    max-width: 64.75rem;
`

const ItemContainer = styled(Container)`
    background-color: #fff;
    border-left: 1px solid rgb(223, 225, 230);
    border-right: 1px solid rgb(223, 225, 230);
    padding: 1.5rem 1rem 2.25rem;
`

const Title = styled(Text)`
    margin-top: 6px;
    font-weight: 600;
    font-size: 26px;
    line-height: 32px;
`

const SubTitle = styled(Text)`
    font-size: 16px;
    line-height: 24px;
    color: #696c71;
`

const Description = styled(Text)`
    line-height: 1.4;
    margin-top: 12px;
`

const GroupList = styled(Container)`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    margin-top: 24px;
    width: 100%;
`

const GroupItem = styled(Container)`
    width: calc(50% - 12px);
    margin-right: 24px
`

const GroupStatus = styled(Text)`
    color: #084;
    margin-bottom: 8px;
    font-weight: 500;
    white-space: nowrap;
    
`

const GroupId = styled(Text)`
    font-weight: 500;
    font-size: 18px;
    line-height: 22px;
    margin-bottom: 8px;
    width: auto;
    margin-right: 4px;
`

const GroupIdNumber = styled(GroupId)`
    color: #717479;
    white-space: nowrap;
`

const Separator = styled.div`
    width: 100%;
    border-bottom: 1px solid #dfe1e6;
    padding-bottom: 40px;
`

const ShowMoreButton = styled(Button)`
    width: 100%;
    background-color: #fff;
    color: #000;
    
    font-weight: 600;
    border-radius: 0.25rem;
    
    &:hover {
        background: #f3f3f3;
    }
`

const StyledInput = styled(Input)`
    border: 0.0625rem solid currentColor;
    border-color: #C4C8D0;
    border-radius: 0.25rem;
    max-width: 45.25rem;
    width: 100%;
    padding-left: 1rem;
    padding-right: 1rem;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    background-color: #fff;
    min-height: 3rem;
    color: #0E0E0F;
    text-overflow: ellipsis;
    background-color: #fff;
    font-size: 1rem;
    color: #0E0E0F;
    text-overflow: ellipsis;
`

const DEFAULT_OFFSET = 10;
const DEFAULT_LIMIT = 10;
const FILTERS = {
    direction: {
        title: 'Направление',
        name: 'type',
        options: [
            {label: 'Все', value: null},
            {label: 'Образование', value: 'Для ума'},
            {label: 'Физическая активности', value: 'Для тела'},
            {label: 'Творчество', value: 'Для духа'}
        ]
    },
    online: {
        title: 'Формат посещения',
        name: 'online',
        options: [
            {label: 'Все', value: null},
            {label: 'Онлайн', value: true},
            {label: 'Офлайн', value: false},
        ]
    },
    area: {
        title: 'Район или метро',
        name: 'area',
        disabled: true,
        options: []
    },
    days: {
        title: 'Дни недели',
        name: 'day',
        options: [
            {label: 'Все', value: null},
            {label: 'Понедельник', value: 1},
            {label: 'Вторник', value: 2},
            {label: 'Среда', value: 3},
            {label: 'Черверг', value: 4},
            {label: 'Пятница', value: 5},
            {label: 'Суббота', value: 6},
            {label: 'Воскресенье', value: 7},
        ]
    },
    times: {
        title: 'Время',
        name: 'time',
        options: [
            {label: 'Все', value: null},
            {label: 'Утро', value: '8:00-10:00'},
            {label: 'Полдень', value: '11:00-13:00'},
            {label: 'День', value: '14:00-16:00'},
            {label: 'Вечер', value: '17:00-19:00'},
        ]
    },
    statuses: {
        title: 'Статус группы',
        name: 'status',
        disabled: true,
        options: [
            {label: 'Все', value: null},
            {label: 'Группа занимается', value: 1},
            {label: 'Ожидание начала занятий', value: 2},
        ]
    }
}

class ListComponent extends Component {
    constructor(props) {
        super(props);

        const {survey_id} = getURLSearchParams();

        this.state = {
            filters: {
                survey_id: survey_id,
                query: '',
                type: null,
                online: null,
                area: null,
                day: null,
                status: null,
                time: null,
                limit: DEFAULT_LIMIT,
                offset: 0,
            },
            items: [],
            total: 0,
        };
    }

    async componentDidMount() {
        await this.searchActivities()
    }

    searchActivities = async () => {
        const {filters} = this.state;
        const activities = await this.props.getActivities(filters);
        if (activities) {
            this.setState({
                total: activities.total,
                items: activities.items,
            })
        }
    }

    loadMore = async () => {
        const {filters, items} = this.state;
        filters.offset += DEFAULT_OFFSET;

        const activities = await this.props.getActivities(filters);

        this.setState({
            filters: filters,
            total: activities.total,
            items: items.concat(activities.items),
        });
    }

    handleChange = async (field, value) => {
        const {filters, items} = this.state
        const updatedFilters = {...filters, [field]: value};

        const activities = await this.props.getActivities(updatedFilters);

        this.setState({
            filters: updatedFilters,
            total: activities.total,
            items: activities.items,
        });
    }

    handleInput = (field, value) => {
        const {filters} = this.state
        const updatedFilters = {...filters, [field]: value};

        this.setState({
            filters: updatedFilters,
        });
    }

    render() {
        const {items, total, filters} = this.state;

        return <Container flow={'column'}>
            <Container flow={'column'} padding={'0 6.4375rem'}>
                <Container margin={'1rem 0 1rem 0'}>
                    <LinkText onClick={() => this.props.goBack()}>&#8592; Назад</LinkText>
                </Container>
                <Container margin={'0 0 1.5rem 0'}>
                    <Text size={'2rem'} lineHeight={'2.5rem'} weight={'600'}>Поиск занятий</Text>
                </Container>
                <Container margin={'24px 0 0 0'} gap={8}>
                    <StyledInput
                        placeholder={'Направление или номер группы'}
                        onChange={(e) => this.handleInput('query', e.currentTarget.value)}
                    />
                    <BlueButton onClick={() => this.searchActivities()}>Найти</BlueButton>
                </Container>
                <Container margin={'1rem 0 0 0'} wrap={'wrap'}>
                    <Filter data={FILTERS.direction} value={filters.type} onChange={this.handleChange}/>
                    <Filter data={FILTERS.online} value={filters.online} onChange={this.handleChange}/>
                    <Filter data={FILTERS.area} value={filters.area} onChange={this.handleChange}/>
                    <Filter data={FILTERS.days} value={filters.day} onChange={this.handleChange}/>
                    <Filter data={FILTERS.times} value={filters.time} onChange={this.handleChange}/>
                    <Filter data={FILTERS.statuses} value={filters.status} onChange={this.handleChange}/>
                </Container>
            </Container>
            <StyledContainer flow={'column'} margin={'24px 0 0 0'}>
                <Container padding={'2rem 0'}>
                    <Text opacity={0.8} size={'16px'}>{'Найдено ' + total + ' активностей'}</Text>
                </Container>
                <ItemsContainer flow={'column'} gap={16}>
                    {items && items.map((item, i) => <ItemContainer key={'list-item_' + i} flow={'column'}
                                                                    width={'100%'}>
                        <Container flow={'column'}>
                            <Title>{item.title}</Title>
                            <Description>{item.description}</Description>
                        </Container>
                        <GroupList>
                            {item.groups.map((group, ii) => <GroupItem flow={'column'}
                                                                       key={'list-item_' + i + '_group_' + ii}>
                                <GroupStatus>{group.status}</GroupStatus>
                                <Container flow={'row'} justify={'flex-start'}>
                                    <GroupId>Группа</GroupId>
                                    <GroupIdNumber>{group.name}</GroupIdNumber>
                                </Container>
                                <Container flow={'column'}>
                                    {group.times && group.times.map((tm, j) => <Container
                                        key={'list-item_' + i + '_group_' + ii + '_tm_' + j}>
                                        <Text>{tm}</Text>
                                    </Container>)}
                                </Container>
                            </GroupItem>)}
                        </GroupList>
                        <Container margin={'24px 0 0 0'}>
                            <BlueButton onClick={() => this.props.changeLocation('/activity?id=' + item.id, true)}>Узнать
                                больше</BlueButton>
                        </Container>
                    </ItemContainer>)}
                    <Container margin={'16px 0 8vh 0'}>
                        {items.length < total && <ShowMoreButton onClick={this.loadMore}>Показать еще</ShowMoreButton>}
                    </Container>
                </ItemsContainer>
            </StyledContainer>
        </Container>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
    }
}

const mapDispatchToProps = {
    push,
    getActivities
}

export const List = connect(mapStateToProps, mapDispatchToProps)(ListComponent);
