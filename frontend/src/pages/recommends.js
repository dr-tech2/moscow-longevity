import React, {Component} from "react";
import {connect} from "react-redux";
import styled from "styled-components";
import {push} from "connected-react-router";
import {Container} from "../components/atoms/container";
import {Text} from "../components/atoms/text";
import {BlueButton, Button, GreenButton} from "../components/atoms/button";
import {LinkText} from "../components/atoms/link-text";
import {getRecommends} from "../actions/service";
import {Icon} from "../components/atoms/icon";
import {Input} from "../components/atoms/input";

const StyledContainer = styled(Container)`
    margin-left: 6.4375rem;
    margin-right: 6.4375rem;
    padding-top: 4rem;
`

const LeadContainer = styled(Container)`
    background-image: url("/img/lead.png");
    background-size: contain;
    background-repeat: no-repeat;
    background-position-x: right;
    background-position-y: calc(100% - 2px);
    width: auto;
    padding-top: 3.0625rem;
    padding-left: 4.5rem;
    padding-bottom: 3.8125rem;
    margin-left: 2.0625rem;
    margin-right: 2.0625rem;
    margin-top: 1rem;
    background-color: #f3f5f7;
    border-radius: 0.5rem;
`

const LeadTitle = styled(Container)`
    margin-left: 0;
    font-size: 2.75rem;
    line-height: 3.3125rem;
    font-weight: 700;
`

const StyledTitle = styled(Text)`
    font-weight: bold;
    font-size: 2rem;
    line-height: 2.5rem;
`

const StyledInput = styled(Input)`
    border-radius: 0.25rem;
    background-color: #fff;
    border: 0.0625rem solid #C4C8D0;
    max-width: 45.25rem;
    width: 100%;
    padding-left: 1rem;
    padding-right: 1rem;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    min-height: 3rem;
    font-size: 1rem;
    color: #0E0E0F;
    text-overflow: ellipsis;
`

const Topics = styled(Container)`
    max-width: 586px;
    box-sizing: border-box;
    width: auto;
    margin-top: 1.75rem;
    margin-bottom: -1.5rem;
`

const Topic = styled(Text)`
    width: auto;
    position: relative;
    color: #0033aa;
    font-weight: bolder;
    cursor: pointer;
    display: inline-block;
    margin-right: 1.5rem;
    margin-bottom: 1.5rem;
    
`

const StyledItemContainer = styled(Container)`
    border: 1px Solid #dfe1e6;
    width: calc(25% - 12px);
    padding: 1.5rem;
`

class RecommendsComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            recommends: this.props.recommends || [],
            query: null,
            topics: [
                {title: 'О проекте', src: 'https://www.mos.ru/city/projects/dolgoletie/'},
                {title: 'Как стать участником', src: 'https://www.mos.ru/city/projects/dolgoletie/map/'},
                {title: 'Центры московского долголетия', src: 'https://www.mos.ru/city/projects/dolgoletie/cmd/'},
                {title: 'Система активного долголетия', src: 'https://www.mos.ru/city/projects/dolgoletie/md/'}
            ]
        }
    }

    async componentDidMount() {
        const {recommends} = this.state;
        if (recommends.length === 0) {
            const activities = await this.props.getRecommends();

            this.setState({recommends: activities})
        }
    }

    openSite = (src) => {
        window.open(src)
    }

    render() {
        const {recommends, topics} = this.state;

        return <Container flow={'column'}>
            <LeadContainer flow={'column'} gap={22}>
                <Container>
                    <Icon type={'logo_longevity'} width={'70px'} height={'106px'} />
                </Container>
                <Container>
                    <LeadTitle>Московское долголетие</LeadTitle>
                </Container>
                <Container width={'420px'}>
                    <Text size={'16px'}>Проект Мэра Москвы для активных москвичей старшего возраста: женщин с 55 и мужчин с 60 лет.</Text>
                </Container>
                <Container width={'30.1875rem'} gap={8}>
                    <StyledInput placeholder={'Направление или номер группы'} onChange={(e) => this.setState({query: e.currentTarget.value})}/>
                    <BlueButton width={'7.0625rem'} height={'3rem'} onClick={() => this.props.changeLocation('/list?query=' + this.state.query)}>Найти</BlueButton>
                </Container>
                <Topics margin={'8px 0 0 0'} wrap={'wrap'}>{topics.map((item, i) =>
                    <Topic onClick={() => item.src && this.openSite(item.src)}>{item.title}</Topic>
                )}</Topics>
            </LeadContainer>
            <StyledContainer flow={'column'}>
                <Container flow={'column'}>
                    <Container>
                        <StyledTitle>Рекомендуемые направления</StyledTitle>
                    </Container>
                    <Container margin={'16px 0 0 0'} minHeight={'48px'} gap={32}>{
                        recommends && recommends.map((item, i) => <StyledItemContainer key={'rec-' + i} flow={'column'}>
                            <Container>
                                <Text bold={true} whiteSpace={'nowrap'}>{item.title}</Text>
                            </Container>
                            <Container flow={'column'} margin={'16px 0 0 0'}>
                                {item.items.map((course, i) => <Container key={'rec-course_' + i} margin={'1rem 0 0 0'}>
                                    <LinkText onClick={() => this.props.changeLocation('/activity?id=' + course.id, true)}>{course.name}</LinkText>
                                </Container>)}
                            </Container>
                        </StyledItemContainer>)
                    }</Container>
                </Container>
                <Container margin={'24px 0 2rem 0'}>
                    <StyledTitle>Поможем вам подобрать интересные занятия</StyledTitle>
                </Container>
                <Container margin={'0 0 0.75rem 0'}>
                    <Text>Пройдите тест и узнайте, какие занятия подходят именно вам</Text>
                </Container>
                <Container  margin={'16px 0 8vh 0'}>
                    <BlueButton onClick={() => this.props.changeLocation('/questions', true)} margin={'0 16px 0 0'}>Подобрать занятие</BlueButton>
                    {/*<BlueButton onClick={() => this.props.changeLocation('/list', true)}>Выбрать из списка</BlueButton>*/}
                </Container>
            </StyledContainer>
        </Container>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
        recommends: state.recommends.items,
    }
}

const mapDispatchToProps = {
    push,
    getRecommends,
}

export const Recommends = connect(mapStateToProps, mapDispatchToProps)(RecommendsComponent)