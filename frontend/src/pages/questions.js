import React, {Component, useEffect, useState} from "react";
import styled, {keyframes} from "styled-components";
import {getURLSearchParams} from "../helpers/url-params-helper";
import {Container} from "../components/atoms/container";
import {Question} from "../components/organisms/question";
import {BlueButton, Button} from "../components/atoms/button";
import {Text} from "../components/atoms/text";
import {getQuestions, createSurvey, getSurvey, storeAnswer, updateSurvey, getPossibleAnswers, getClient} from "../actions/service";
import {push} from "connected-react-router";
import {connect} from "react-redux";
import {ContentLoader} from "../components/atoms/content-loader";
import {Popup} from "../components/molecules/popup";

const StyledContainer = styled(Container)`
    min-height: 225px;
    background-color: #f3f5f7;
    position: relative;
    padding-bottom: 5rem;
    padding-top: 1.75rem;
    justify-content: center;
    align-items: center;
`

const QuestionContainer = styled(Container)`
    background-color: rgb(255, 255, 255);
    border-radius: 0.5rem;
    border: 1px solid rgb(223, 225, 230);
    padding: 32px 32px 24px;
    max-width: 760px;
`

const STATUS_SURVEY_NEW = 1;
const STATUS_SURVEY_FINISHED = 2;

class QuestionsComponent extends Component {
    constructor(props) {
        super(props);

        const {question_id, survey_id} = getURLSearchParams();

        this.state = {
            client: null,
            popupParams: {},
            survey_id: survey_id || null,
            question_id: question_id ? Number(question_id) : 1,
            answers: {},
            questions: [],
        };
    }

    async componentDidMount() {
        const {survey_id, question_id} = this.state;
        const survey = survey_id
            ? await this.props.getSurvey({survey_id: survey_id})
            : await this.props.createSurvey();

        const questions = await this.props.getQuestions();
        const query = new URLSearchParams({survey_id: survey.id, question_id: question_id})
        this.props.changeLocation('/questions?' + query);

        let answers = {}
        survey.answers && survey.answers.map(item => {
            try {
                answers[item.question_id] = JSON.parse(item.answer);
            } catch (e) {
                answers[item.question_id] = item.answer;
            }
        })

        const question = questions.find(item => item.id === question_id)
        if (question && question.load_answers) {
            const possibleAnswers = await this.props.getPossibleAnswers({
                question_id: question_id,
                survey_id: survey_id,
            });

            question.answers = possibleAnswers.map(item => item.answer)
            question.descriptions = {}
            possibleAnswers.forEach(item => question.descriptions[item.answer] = item.description)
        }

        let client = survey_id ? await this.props.getClient({survey_id: survey_id}) : null;

        this.setState({
            answers: answers,
            questions: questions,
            survey_id: survey.id,
            client: client,
        })
    }

    /**
     * Следующий вопрос
     *
     * @returns {Promise<void>}
     */
    nextQuestion = async () => {
        const {question_id, survey_id, answers, questions, client} = this.state;
        const question = questions.find(item => item.id === question_id)

        await this.props.storeAnswer({
            survey_id: survey_id,
            answer: JSON.stringify(answers[question_id]),
            question_id: question_id,
            possible_answers: question.answers,
        });

        let next = question_id + 1;
        if (question_id === 1 && !isNaN(answers[1])) {
            next = question_id + 2;
        }

        const nextQuestion = questions.find(item => item.id === next)
        if (nextQuestion && !!nextQuestion.load_answers) {
            const possibleAnswers = await this.props.getPossibleAnswers({
                question_id: next,
                survey_id: survey_id,
            });

            nextQuestion.answers = possibleAnswers.map(item => item.answer)
            nextQuestion.descriptions = {}
            possibleAnswers.forEach(item => nextQuestion.descriptions[item.answer] = item.description)
        }

        let uploadedClient = client;
        //проверка корректности адреса
        if (next === 3 && survey_id) {
            uploadedClient = await this.props.getClient({survey_id: survey_id});
        }

        const query = new URLSearchParams({survey_id: survey_id, question_id: next})
        this.props.changeLocation('/questions?' + query);

        this.setState({
            question_id: next,
            client: uploadedClient,
        })
    }

    /**
     * Предыдущий вопрос
     */
    prevQuestion = async () => {
        const {question_id, survey_id, questions, answers} = this.state;

        let prev = question_id - 1;
        if (question_id === 3 && !isNaN(answers[1])) {
            prev = question_id - 2;
        }

        const query = new URLSearchParams({survey_id: survey_id, question_id: prev})
        this.props.changeLocation('/questions?' + query);

        const question = questions.find(item => item.id === prev)
        if (question && question.load_answers && !question.answers) {
            const possibleAnswers = await this.props.getPossibleAnswers({
                question_id: question_id,
                survey_id: survey_id,
            });

            question.answers = possibleAnswers.map(item => item.answer)
            question.descriptions = {}
            possibleAnswers.forEach(item => question.descriptions[item.answer] = item.description)
        }

        this.setState({
            question_id: prev,
        })
    }

    /**
     * Сохранение ответа в БД
     *
     * @param question
     * @param selectedItems
     * @param item
     */
    storeAnswer = (question, selectedItems, item) => {
        const {answers} = this.state;

        this.setState({
            answers: {...answers, [question.id]: selectedItems}
        })
    }

    /**
     * Завершение опроса
     *
     * @returns {Promise<void>}
     */
    finishQuestions = async () => {
        const {survey_id} = this.state;

        await this.props.updateSurvey({
            survey_id: survey_id,
            status_id: STATUS_SURVEY_FINISHED,
        })

        this.props.changeLocation('/list?survey_id=' + survey_id, true);
    }

    setPopupParams = (params) => {
        this.setState({
            popupParams: params,
        })
    }

    render() {
        const {questions, question_id, answers, survey_id, popupParams, client} = this.state;
        const question = questions.find(item => item.id === question_id);

        const isDisabled = (this.props.isLoading || (question && !answers[question.id])) ? {disabled: true} : {}
        const isPrevDisabled = this.props.isLoading ? {disabled: true} : {}

        return <Container flow={'column'}>
            <StyledContainer flow={'column'}>
                {question && <QuestionContainer flow={'column'}>
                    <Container>
                        <Text color={'#696c71'}>{`Шаг ${question_id} из ${questions.length}`}</Text>
                    </Container>
                    <Container margin={'16px 0 0 0'}>
                        <Question
                            survey_id={survey_id}
                            mobile={this.props.mobile}
                            question={question}
                            answers={answers}
                            storeAnswer={this.storeAnswer}
                            setPopupParams={this.setPopupParams}
                            nextQuestion={this.nextQuestion}
                            client={client}
                        />
                    </Container>
                    <Container margin={'33px 0 0 0'} justify={'flex-start'} gap={8}>
                        {question_id !== 1 && <BlueButton onClick={this.prevQuestion} {...isPrevDisabled} margin={'0 16px 0 0'}>Назад</BlueButton>}
                        {question_id !== questions.length
                            ? <BlueButton onClick={this.nextQuestion} {...isDisabled} margin={'0 16px 0 0'}>Далее</BlueButton>
                            : <BlueButton onClick={this.finishQuestions} {...isDisabled} margin={'0 16px 0 0'}>Отправить</BlueButton>}
                        {this.props.isLoading && <ContentLoader />}
                    </Container>
                </QuestionContainer>}
            </StyledContainer>
            <Popup {...popupParams} />
        </Container>
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
        isLoading: state.main.isLoading,
    }
}

const mapDispatchToProps = {
    push,
    getQuestions,
    createSurvey,
    getSurvey,
    storeAnswer,
    updateSurvey,
    getPossibleAnswers,
    getClient,
}

export const Questions = connect(mapStateToProps, mapDispatchToProps)(QuestionsComponent)