import React, {useState} from "react";
import styled from "styled-components";
import {Container} from "../atoms/container";
import {Input} from "../atoms/input";
import {StyledInputWithMask} from "../atoms/styled-input-with-mask";
import {CheckboxWithLabel} from "../atoms/checkbox-with-label";
import {Text} from "../atoms/text";

export const Address = ({address, onChange, client, mobile, nextQuestion}) => {
    const NAME = 'is_address_correct';
    const [isAddressCorrect, setIsAddressCorrect] = useState(null);
    const parsed = 'object' === typeof address ? address : JSON.parse(address)
    const parsedIsEmpty = !parsed || Object.keys(parsed).length === 0;
    const clientIsEmpty = !client || Object.keys(client).length === 0;

    const handleChange = (name, value) => {
        onChange({...parsed, [name]: value})
    }

    const handleIsAddressCorrect = (value) => {
        setIsAddressCorrect(value)
        onChange(value === true ? client.address : {})

        value === true && nextQuestion();
    }

    return <Container flow={'column'} gap={16}>
        {!clientIsEmpty && parsedIsEmpty && <Container flow={'column'} gap={8}>
            <Container>
                <Text>Корректен ли адрес: {client.address_raw}?</Text>
            </Container>
            <Container gap={16}>
                <CheckboxWithLabel
                    label={'Да'}
                    name={NAME}
                    checked={isAddressCorrect === true}
                    onClick={() => handleIsAddressCorrect(true)}
                    mobile={mobile}
                />
                <CheckboxWithLabel
                    label={'Нет'}
                    name={NAME}
                    checked={isAddressCorrect === false}
                    onClick={() => handleIsAddressCorrect(false)}
                    mobile={mobile}
                />
            </Container>
        </Container>}
        {(clientIsEmpty || isAddressCorrect === false || !parsedIsEmpty) && <Container flow={'column'} gap={8}>
            <Container>
                <Input
                    value={parsed.street || ''}
                    width={'316px'}
                    placeholder={'Улица'}
                    onChange = {(e) => handleChange('street', e.currentTarget.value)}
                />
            </Container>
            <Container flow={'row'} gap={8}>
                <Input
                    value={parsed.house || ''}
                    width={'100px'}
                    placeholder={'Дом'}
                    onChange = {(e) => handleChange('house', e.currentTarget.value)}
                />
                <StyledInputWithMask
                    value={parsed.building || ''}
                    width={'100px'}
                    placeholder={'Строение'}
                    height={'42px'}
                    mask={'aaaaa'}
                    formatChars={{'a': '[0-9a-zA-Zа-яёА-Яё]'}}
                    maskChar={''}
                    onChange = {(e) => handleChange('building', e.currentTarget.value)}
                />
                <StyledInputWithMask
                    value={parsed.construction || ''}
                    width={'100px'}
                    placeholder={'Корпус'}
                    height={'42px'}
                    mask={'aaaaaaaaa'}
                    formatChars={{'a': '[0-9a-zA-Zа-яёА-Яё]'}}
                    maskChar={''}
                    onChange = {(e) => handleChange('construction', e.currentTarget.value)}
                />
            </Container>
        </Container>}
    </Container>
}