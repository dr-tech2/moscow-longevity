import React, {useState} from "react";
import {Container} from "../atoms/container";
import {StyledInputWithMask} from "../atoms/styled-input-with-mask";
import {CheckboxWithLabel} from "../atoms/checkbox-with-label";

export const Fio = ({value, onChange, placeholder, mobile}) => {
    const NAME = 'type_identify';

    const [typeIdentify, setTypeIdentify] = useState(isNaN(value) ? 1 : 2);

    return <Container flow={'column'} gap={24}>
        <Container gap={16}>
            <CheckboxWithLabel
                label={'ФИО'}
                name={NAME}
                checked={typeIdentify === 1}
                onClick={() => setTypeIdentify(1)}
                mobile={mobile}
            />
            <CheckboxWithLabel
                label={'ID пользователя'}
                name={NAME}
                checked={typeIdentify === 2}
                onClick={() => setTypeIdentify(2)}
                mobile={mobile}
            />
        </Container>
        <Container>
            {typeIdentify === 1 && <StyledInputWithMask
                width={'420px'}
                type={'text'}
                height={'42px'}
                placeholder={placeholder}
                value={isNaN(value) ? value : null}
                onChange={onChange}
                mask={'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'}
                formatChars={{'a': '[а-яёА-Яё \-]'}}
                maskChar={''}
            />
            }
            {typeIdentify === 2 && <StyledInputWithMask
                width={'220px'}
                type={'text'}
                height={'42px'}
                placeholder={'ID пользователя'}
                mask={'aaaaaaaaaaaaaaaaaaaa'}
                formatChars={{'a': '[0-9]'}}
                maskChar={''}
                value={!isNaN(value) ? value : null}
                onChange={onChange}/>
            }
        </Container>
    </Container>
}