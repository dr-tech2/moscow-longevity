import React, {useState} from "react";
import styled from "styled-components";
import {Container} from "../atoms/container";
import Select from "react-select/base";

export const Filter = ({data, value, onChange}) => {
    const [selectedValue, setSelectedValue] = useState()
    const [inputValue, setInputValue] = useState()
    const [menuInOpen, setMenuIsOpen] = useState()

    const colourStyles = {
        placeholder: (styles, {isDisabled}) => {
            return {
                ...styles,
                color: isDisabled ? '#b7babd' : '#0033aa',
            }
        },
        control: (styles, {isDisabled}) => ({
            ...styles,
            border: 'none',
            backgroundColor: isDisabled ? '#f3f5f7' : '#dde7ff',
            color: isDisabled ? '#b7babd' : '#0033aa',
            fontSize: '17px',
            whiteSpace: 'nowrap',
            fontWeight: 500
        }),
        singleValue: styles => ({
            ...styles,
            color: '#0033aa',
        }),
        dropdownIndicator: (styles, {isDisabled}) => ({
            ...styles,
            color: isDisabled ? '#b7babd' : '#0033aa',
        }),
        option: (styles, {data, isDisabled, isFocused, isSelected}) => {
            return {
                ...styles,
                cursor: 'pointer',
                color: '#0e0e0f',
                fontWeight: 500,
                backgroundColor: (isSelected || isFocused) ? '#dde7ff' : '#fff',
                fontSize: '17px'
            };
        }
    };

    return <Container flow={'column'} width={'220px'} padding={'0 12px 16px 0'}>
        <Select
            components={{
                IndicatorSeparator: () => null
            }}
            styles={colourStyles}
            options={data.options}
            value={selectedValue}
            onChange={(item) => {
                setSelectedValue(item)
                onChange(data.name, item.value)
            }}
            inputValue={inputValue}
            onInputChange={(inputValue) => setInputValue(inputValue)}
            placeholder={data.title}
            isDisabled={data.disabled}
            defaultMenuIsOpen={true}
            menuIsOpen={menuInOpen}
            onMenuOpen={() => setMenuIsOpen(true)}
            onMenuClose={() => setMenuIsOpen(false)}
        />
    </Container>
}