import React, {useState} from "react";
import {Container} from "../atoms/container";
import {CheckboxWithLabel} from "../atoms/checkbox-with-label";
import {Text} from "../atoms/text";
import {InputDate} from "../molecules/date";
import {Input} from "../atoms/input";
import {Address} from "./address";
import {chuckArray} from "../../helpers/array-functions";
import {CheckboxWithImage} from "../atoms/checkbox-with-image";
import {Fio} from "./fio";

export const Question = (props) => {
    const {question, mobile, answers, storeAnswer, setPopupParams, client, nextQuestion} = props;
    const NO_ONE = 'Затрудняюсь ответить';

    const onSelectHandler = (id, item, type) => {
        switch (type) {
            case 'checkbox':
                let selectedItems = answers[question.id] ? answers[question.id] : [];
                if (selectedItems.includes(item)) {
                    storeAnswer(question, selectedItems.filter(i => i !== item && i !== NO_ONE), item);
                } else {
                    if (item === NO_ONE) {
                        storeAnswer(question, [item], item);
                    } else {
                        storeAnswer(question, [...selectedItems.filter(i => i !== NO_ONE), item], item);
                    }
                }
                break;
            default:
                storeAnswer(question, item, item)
                break;
        }
    }

    const getCheckbox = () => {
        return question.answers && question.answers.map((item, i) => <Container key={'question-answer-' + i}>
            <CheckboxWithLabel
                label={item}
                name={question.name}
                checked={answers[question.id] && answers[question.id].includes(item)}
                onClick={() => onSelectHandler(question.id, item, 'checkbox')}
                mobile={mobile}
            />
        </Container>)
    }

    const getCheckboxImages = () => {
        const chunk = chuckArray(question.answers, 2)

        return <Container>{chunk.map(items =>
            <Container flow={'column'} gap={8}>{items.map((item, i) => {
                const checked = answers[question.id] && answers[question.id].includes(item)
                const description = question.descriptions[item];

                return <Container
                    key={'question-answer-' + i}
                    align={'center'}
                    checked={checked}
                    gap={8}
                >
                    <CheckboxWithImage
                        label={item}
                        description={description}
                        name={question.name}
                        checked={checked}
                        onClick={() => onSelectHandler(question.id, item, 'checkbox')}
                        setPopupParams={setPopupParams}
                        mobile={mobile}
                        justify={'center'}
                    />
                </Container>
            })}</Container>
        )}</Container>
    }

    const getRadio = () => {
        return question.answers && question.answers.map((item, i) => <Container key={'question-answer-' + i}>
            <CheckboxWithLabel
                label={item}
                name={question.name}
                checked={answers[question.id] === item}
                onClick={() => onSelectHandler(question.id, item, question.type)}
                mobile={mobile}
            />
        </Container>)
    }

    const getText = () => {
        return <Container flow={'column'} gap={8}>
            <Fio
                placeholder={question.placeholder}
                value={answers[question.id] || ''}
                onChange={(e) => onSelectHandler(question.id, e.currentTarget.value)}
                mobile={mobile}
            />
        </Container>
    }

    const getDate = () => {
        return <Container gap={8}>
            <InputDate
                selected={answers[question.id] || ''}
                onChange={(value) => onSelectHandler(question.id, value, question.type)}
            />
        </Container>
    }

    const getAddress = () => {
        return <Container gap={8}>
            <Address
                client={client}
                address={answers[question.id] || {}}
                onChange={(value) => onSelectHandler(question.id, value, question.type)}
                mobile={mobile}
                nextQuestion={nextQuestion}
            />
        </Container>
    }

    return <Container shrink={'0'} flow={'column'}>
        <Container flow={'column'}>
            <Text size={'1.2rem'} weight={600}>{question.title}</Text>
        </Container>
        <Container flow={'column'} margin={'18px 0 0 0'}>
            {question.type === 'checkbox' && getCheckbox()}
            {question.type === 'checkbox-images' && getCheckboxImages()}
            {question.type === 'radio' && getRadio()}
            {question.type === 'text' && getText()}
            {question.type === 'date' && getDate()}
            {question.type === 'address' && getAddress()}
        </Container>
    </Container>
}