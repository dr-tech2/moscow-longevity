import React, {Component} from "react";
import styled from "styled-components";
import {Container} from "./atoms/container";
import {GreenButton} from "./atoms/button";

const StyledContainer = styled(Container)`
    margin: 50px 0;
    vertical-align: baseline;
    font-size: 40px;
    line-height: 48px;
    font-weight: 700;
`

const TitleButton = styled(GreenButton)`
    position: relative;
`

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            buttons: [
                {title: 'О проекте', src: 'https://www.mos.ru/city/projects/dolgoletie/'},
                {title: 'Как стать участником', src: 'https://www.mos.ru/city/projects/dolgoletie/map/'},
                {title: 'Центры московского долголетия', src: 'https://www.mos.ru/city/projects/dolgoletie/cmd/'},
                {title: 'Система активного долголетия', src: 'https://www.mos.ru/city/projects/dolgoletie/md/'}
            ]
        };
    }

    openSite = (src) => {
        window.open(src)
    }

    render() {
        const {buttons} = this.state;

        return <Container flow={'column'}>
            <Container margin={'8px 0 0 0'} wrap={'wrap'}>{buttons.map((item, i) =>
                <TitleButton
                    key={'header-button-' + i}
                    margin={'16px 16px 0 0'}
                    onClick={() => item.src && this.openSite(item.src)}>{
                        item.title
                    }</TitleButton>
            )}</Container>
            <StyledContainer>
                Поиск занятий
            </StyledContainer>
        </Container>
    }
}

export default Header;