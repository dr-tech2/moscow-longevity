import React, {Component} from "react";
import styled from "styled-components";
import {Container} from "../atoms/container";
import {Text} from "../atoms/text";
import {Checkbox} from "../atoms/checkbox";
import {CheckboxWithLabel} from "../atoms/checkbox-with-label";
import {MenuItemTitle} from "../atoms/menu-item-title";

const StyledContainer = styled(Container)`
    margin-top: 14px;
    max-height: 320px;
    overflow: auto;
`

const ListItem = styled(Container)`
    margin-bottom: 16px;
    position: relative;
    cursor: pointer;
`

export const Days = ({}) => {
    const days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];

    return <Container flow={"column"} margin={'40px 0 0 0'}>
        <MenuItemTitle>День недели</MenuItemTitle>
        <StyledContainer flow={"column"}>
            {days && days.map((item, i) => <ListItem key={'day_'+ i}>
                <CheckboxWithLabel
                    label={item}
                    name={item}
                    checked={false}
                    disabled={false}
                    onClick={() => {}}
                />
            </ListItem>)}
        </StyledContainer>
    </Container>
}