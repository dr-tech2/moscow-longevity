import React from "react";
import DatePicker from "react-datepicker";
import {StyledInputWithMask} from "../atoms/styled-input-with-mask";
import ru from 'date-fns/locale/ru';

import "react-datepicker/dist/react-datepicker.css"
import "./datepicker.css"

/**
 * Преобразование даты в строку
 *
 * @param date
 *
 * @returns {string|null}
 */
const getFormattedDate = (date) => {
    if (!date)
        return null

    const yyyy = date.getFullYear()
    let mm = date.getMonth() + 1
    let dd = date.getDate()

    if (dd < 10) dd = '0' + dd
    if (mm < 10) mm = '0' + mm

    return [dd, mm, yyyy].join('.')
}

/**
 * Преобразование строки в дату
 *
 * @param date
 *
 * @returns {Date}
 */
const toDateFormat = (date) => {
    const spl = date.split('.')
    if (!spl || spl.length < 3)
        return null

    const [dd, mm, yy] = date.split('.')

    return new Date(yy, mm - 1, dd)
}
/**
 * Молекула Date
 *
 * @param selected
 * @param onChange
 * @param isDisabled
 * @returns {JSX.Element}
 *
 * @constructor
 */
export const InputDate = ({selected, onChange, isDisabled}) => {
    const handleChange = (date) => {
        const formattedDate = getFormattedDate(date)
        onChange(formattedDate)
    };

    return <DatePicker
        showIcon
        portalId="root-portal"
        showYearDropdown
        showMonthDropdown
        yearDropdownItemNumber={90}
        scrollableYearDropdown
        selected={selected ? toDateFormat(selected) : null}
        onChange={(date) => handleChange(date)}
        maxDate={new Date()}
        dateFormat="dd.MM.yyyy"
        locale={ru}
        placeholderText="Укажите дату"
        customInput={<StyledInputWithMask
            width={'200px'}
            height={'42px'}
            mask={'aa.aa.aaaa'}
            formatChars={{'a': '[0-9]'}}
            maskChar={''}
        />
        }
        {...(isDisabled ? {disabled: 'disabled'} : {})}
    />;
}

