import React, {Component} from "react";
import styled from "styled-components";
import {Container} from "../atoms/container";
import {Text} from "../atoms/text";
import {CheckboxWithLabel} from "../atoms/checkbox-with-label";
import {LinkText} from "../atoms/link-text";
import {MenuItemTitle} from "../atoms/menu-item-title";
import {Button} from "../atoms/button";
import {Icon} from "../atoms/icon";

const StyledContainer = styled(Container)`
    margin-top: 14px;
    max-height: 320px;
    overflow: auto;
`

const ListItem = styled(Container)`
    display: flex;
    align-items: center;
    margin-bottom: 16px;
    position: relative;
    cursor: pointer;
`

const ShowAllText = styled(Text)`
    color: #084;
    outline: none;
    margin-top: 14px;
    cursor: pointer;
`

const ArrowDown = styled.span`
    display: inline-block;
    background-repeat: no-repeat;
    width: 12px;
    height: 7px;
    position: absolute;
    right: 0;
    top: 10px;
`

export const Direction = ({list}) => {
    const directions = ['Английский язык', 'Гимнастика', 'Домоводство', 'Здорово жить', 'Иностранные языки'];

    return <Container flow={"column"} margin={'40px 0 0 0'}>
        <MenuItemTitle>Направление</MenuItemTitle>
        <StyledContainer flow={'column'}>
            {directions.map((item, i) => <ListItem key={'dir_' + i}>
                    <CheckboxWithLabel
                        label={item}
                        name={item}
                        checked={false}
                        disabled={false}
                        onClick={() => {}}
                    />
                    <ArrowDown>
                        <Icon type={'arrow_down'} color={'#b2b5bb'} />
                    </ArrowDown>
                </ListItem>
            )}
        </StyledContainer>
        <ShowAllText>Показать все</ShowAllText>
    </Container>
}