import React from "react";
import styled from "styled-components";
import {Container} from "../atoms/container";

const PopupContainer = styled(Container)`
    display: block;
    position: fixed;
    left: ${p => p.left ? p.left : 0}px;
    top: ${p => p.top ? p.top : 0}px;
    max-width: 350px;
    width: auto
    height: auto;
    background-color: #fff;
    padding: 16px;
    -webkit-box-shadow: -1px 5px 17px 5px rgba(0,0,0,0.2); 
    box-shadow: -1px 5px 17px 5px rgba(0,0,0,0.37);
`

export const Popup = ({visible, content, position, style}) => {
    return visible && <PopupContainer {...position}>
        {content}
    </PopupContainer>
}