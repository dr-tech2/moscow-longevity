import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'
import {Container} from "../atoms/container";
import {Logo} from "../atoms/logo";
import {Text} from "../atoms/text";
import {Icon} from "../atoms/icon";
import {Footer} from "../footer";

const BaseContainer = styled(Container)`
  position: relative;
  min-height: 100vh;
  background-color: #ced0d2;

  @media (max-width: 767px) {
    min-height: 100%;
  }
`

const ContentContainer = styled(Container)`
  position: relative;
  overflow-y: auto;
  overflow-x: hidden;
  height: 100%;
  background-color: #fff;

  margin: 0 auto;
  max-width: 1600px;

  @media (max-width: 767px) {
    width: 100%;
    min-height: 100%;
  }
`

const Banner = styled(Container)`
    height: 40px;
    line-height: 40px;
    width: 100%;
    background-color: #cc2222;
    z-index: 100;
    color: #fff;
    padding: 0 24px;
`

const Menu = styled(Container)`
    display: flex;
    width: auto;
    justify-content: flex-start;
    align-items: center;
    padding-left: 33px;
    padding-right: 33px;
    border-bottom: 1px Solid #e2e8f0;
`

const MenuItem = styled(Text)`
     white-space: nowrap;
     width: auto;
     font-size: 18px;
     line-height: 56px;
     cursor: pointer;
     text-size-adjust 100%;
     
     &:hover {
        color: #cc2222;
     }
`

const Breadcrumbs = styled(Container)`
    display: flex;
    width: auto;
    justify-content: flex-start;
    align-items: center;
    padding-left: 33px;
    padding-right: 33px;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
`

const Breadcrumb = styled(Text)`
    width: auto;
    text-decoration: none;
    color: #696C71;
    font-size: 0.875rem;
    line-height: 1.5;
`

class BaseTemplatePure extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {

        };
    }

    render() {
        const {children, mobile} = this.props
        return (
            <BaseContainer flow={'column'} align={'stretch'} shrink={'0'} justify={mobile ? 'flex-start' : 'center'}>
                <ContentContainer shrink={'0'} flow={'column'}>
                    <Banner flow={'row'} justify={'space-between'}>
                        <Container>
                            <Container align={'center'} gap={8} width={'auto'}>
                                <Icon type={'logo_mosru'}/>
                                <Text weight={500} color={'#fff'} size={'18px'}>mos.ru</Text>
                            </Container>
                            <Container width={'auto'}>
                                <Text color={'#fff'} size={'16px'} whiteSpace={'nowrap'}>Официальный сайт Мэра
                                    Москвы</Text>
                            </Container>
                        </Container>
                        <Container align={'center'} gap={8} width={'auto'}>
                            <Icon type={'human'}/>
                            <Text color={'#fff'} size={'16px'} whiteSpace={'nowrap'}>Гость</Text>
                        </Container>
                    </Banner>
                    <Menu gap={28}>
                        <MenuItem>Новости</MenuItem>
                        <MenuItem>Афиша</MenuItem>
                        <MenuItem>Услуги</MenuItem>
                        <MenuItem>Мэр</MenuItem>
                        <MenuItem>Власть</MenuItem>
                        <MenuItem>Карта</MenuItem>
                        <MenuItem>Мой район</MenuItem>
                        <MenuItem>Помощь</MenuItem>
                    </Menu>
                    <Breadcrumbs gap={16}>
                        <Breadcrumb>Услуги</Breadcrumb>
                        <Breadcrumb>></Breadcrumb>
                        <Breadcrumb>Московское долголетие</Breadcrumb>
                    </Breadcrumbs>
                    <Container>
                        {children}
                    </Container>
                    <Footer />
                </ContentContainer>
            </BaseContainer>
        )
    }
}

const mapStateToProps = state => {
    return {
        mobile: state.main.mobile,
    }
}

export const BaseTemplate = connect(mapStateToProps, null)(BaseTemplatePure)
