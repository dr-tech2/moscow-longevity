import React, { Component }  from 'react'
import styled, { keyframes } from 'styled-components'

const rotate = keyframes`
  100% {
    transform: rotate(360deg);
  }
`

const dash = keyframes`
  0% {
    stroke-dasharray: 1, 200;
    stroke-dashoffset: 0;
  }
  50% {
    stroke-dasharray: 89, 200;
    stroke-dashoffset: -35px;
  }
  100% {
    stroke-dasharray: 89, 200;
    stroke-dashoffset: -124px;
  }
`

const LoaderBg = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
`

const LoaderDiv = styled.div`
  z-index: 101;
`

const LoaderSvg = styled.svg`
  animation: ${rotate} 2s linear infinite;
  height: 42px;
  transform-origin: center center;
  width: 42px;
  & circle {
    stroke-dasharray: 2, 200;
    stroke-dashoffset: 0;
    animation: ${dash} 1.5s ease-in-out infinite;
    stroke-linecap: round;
    stroke: #0044cc;
  }
`

export const ContentLoader = (props) => {
  return (
    <LoaderBg>
      <LoaderDiv>
        <LoaderSvg className="loader">
          <svg viewBox={'25 25 50 50'}>
            <circle cx={'50'}
              cy={'50'}
              r={'20'}
              fill="none"
              strokeWidth="1"
              strokeMiterlimit="10"
            />
          </svg>
        </LoaderSvg>
      </LoaderDiv>
    </LoaderBg>
  )
}
