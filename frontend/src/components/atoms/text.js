import styled, { css }            from 'styled-components'
import PropTypes                  from 'prop-types'
import {font, color} from "../theme";

export const Text = styled.div`
  display: block;
  font-size: ${props => props.size};
  font-family: ${props => font[props.family]};
  color:  ${props => props.color};
  line-height: ${props => props.lineHeight};
  text-align: ${props => props.align};
  width:  ${props => props.width};

  ${(p) => p.shrink && css`
    flex-shrink: ${p.shrink};
  `}
  
  ${(p) => p.opacity && css`
    opacity: ${p.opacity};
  `}

  ${(p) => p.uppercase && css`
    text-transform: uppercase;
  `}

  ${(p) => p.height && css`
    height: ${p.height};
  `}

  ${(p) => p.ellipsis && css`
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  `}

  ${(p) => p.pointer && css`
    cursor: pointer;
  `}

  ${(p) => p.padding && css`
    padding: ${p.padding};
  `}

  ${(p) => p.bold && css`
    font-weight: bold;
  `}

  ${(p) => p.wrap && css`
    word-break: break-word;
  `}

  ${(p) => p.whiteSpace && css`
    white-space: ${p.whiteSpace};
  `}
  ${(p) => p.weight && css`
    font-weight: ${p.weight};
  `}
`

Text.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string,
  bold: PropTypes.bool,
  hideOverflow: PropTypes.bool,
  align: PropTypes.string,
  width: PropTypes.string,
  shrink: PropTypes.string,
  padding: PropTypes.string,
  letterSpacing: PropTypes.string,
  family: PropTypes.string,
  weight: PropTypes.string
}

Text.defaultProps = {
  bold: false,
  margin: null,
  size: font.textSizeDefault,
  color: font.textColorDefault,
  align: 'left',
  width: '100%',
  family: 'pt',
}
