import styled from "styled-components";
import {Input} from "./input";

export const RadioInput = styled(Input)`
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;

      display: inline-block;
      min-width: 25px;
      max-width: 25px;
      height: 25px;
      padding: 6px;

      background-clip: content-box;
      border: 1px solid #bbbbbb;
      background-color: #ffffff;
      border-radius: 50%;
      cursor: pointer;

      &:focus {
        border: 2px solid #4D9898;
        box-shadow: inset 0 0 0 0.6em #4D9898;
        border: none;
      }

     &:checked {
        border-color: #4D9898;
        box-shadow: inset 0 0 0 0.6em #4D9898;
        border: none
     }
`