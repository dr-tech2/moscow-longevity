import React, {PureComponent} from 'react'
import styled, {css} from 'styled-components'
import {Container} from "./container";
import {Text} from "./text";
import {Icon} from "./icon";

const Input = styled.input`
  opacity: 0;
`
const Checkbox = styled.div`
  display: inline-block;
  position: relative;
  margin-left: -17px !important;
  width: 20px;
  height: 20px;
  transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
  flex-shrink: 0;
  cursor: pointer;

  ${(p) => p.checked && `
    &:after {
      display: inline-block;
      position: absolute;
      width: 20px;
      height: 20px;
      left: 0;
      top: 0;
      padding-left: 3px;
      font-size: 11px;
      color: #ffffff;
      font-family: "FontAwesome";
    }
  `}
`

export class CheckboxWithLabel extends PureComponent {
    constructor(props) {
        super(props)
    }

    onClick = () => {
        // this.input.focus()
        !this.props.disabled && this.props.onClick()
    }

    render() {
        const {label, name, checked, disabled, mobile, justify, color} = this.props

        return (
            <Container
                flow={'row'}
                gap={10}
                width={'auto'}
                height={'24px'}
                margin={'22px 0 0 0'}
                align={'center'}
                onClick={this.onClick}
                justify={justify}
            >
                <Input type={'checkbox'} name={name} innerRef={(field) => {
                    this.input = field
                }} checked={checked} disabled={disabled}/>
                <Checkbox checked={checked} disabled={disabled} align={'center'}>
                    <Icon type={checked ? 'circle_checked' : 'circle'} color={checked ? '#0044cc' : '#c4c8d0'} height={'16px'}/>
                </Checkbox>
                <Text size={'15px'} lineHeight={'1'}>{label}</Text>
            </Container>
        )
    }
}
