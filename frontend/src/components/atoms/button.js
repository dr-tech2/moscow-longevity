import styled, { css }                            from 'styled-components'
import PropTypes                   from 'prop-types'
import {color, hoverColor, hoverBorder, outlineBorder, noTextSelection, buttonColor} from "../theme";

export const Button = styled.button`  
  display: inline-block;
  margin: ${(p) => p.margin ? p.margin : '0'};
  font-weight: 400;
  line-height: 1.42857143;
  text-align: center;
  white-space: ${(p) => p.space ? p.space : 'nowrap'};
  vertical-align: middle;
  touch-action: manipulation;
  cursor: pointer;
  background-image: none;
  color: ${(p) => p.btnType !== 'outline' && p.btnType !== 'outlineRounded' && p.btnType !== 'outlineSquare' && p.color !== 'default' ? color['white'] : buttonColor[p.color]};
  transition: all 0.3s ease;
  outline: none;
  box-shadow: none;
  transition: all 0.3s ease;
  background: ${(p) => p.color && p.btnType !== 'outline' && p.btnType !== 'outlineRounded' && p.btnType !== 'outlineSquare' ? color[p.color] : 'none'};
  border: solid 1px ${(p) => p.color ? p.btnType !== 'outline' && p.btnType !== 'outlineRounded' ? color[p.color] : outlineBorder[p.color] : 'transparent'};
  
  &:hover {
    background: ${(p) => p.btnType !== 'outline' && p.btnType !== 'outlineRounded' ? hoverColor[p.color] : 'none'};
    border: solid 1px ${(p) => p.btnType !== 'outline' && p.btnType !== 'outlineRounded' ? hoverBorder[p.color] : color[p.color]};
  }
  
  ${(p) => {
    switch(p.btnType) {
        case 'rounded':
            return `
          border-radius: 60px;
        `
        case 'outline':
            return `
          border-radius: 2px;
        `
        case 'outlineRounded':
            return `
          border-radius: 60px;
        `
        case 'square':
            return `
          width: 42px;
          height: 42px;
          border-radius: 2px;
          padding: 0 !important;
          vertical-align: middle;
          position: relative;
          overflow: hidden;
          z-index: 1;
          background: transparent !important;
          box-shadow: 0 0 0 35px ${color[p.color]} inset;
          
          &:hover {
            color: ${color[p.color]};
            box-shadow: 0 0 0 1px ${color[p.color]} inset;
            border: none !important;
          }
        `
        case 'outlineSquare':
            return `
          width: 42px;
          height: 42px;
          border-radius: 2px;
          padding: 0 !important;
          vertical-align: middle;
          position: relative;
          overflow: hidden;
          box-shadow: 0 0 0 1px ${color[p.color]} inset;
          border: none !important;
          z-index: 1;
          
          &:hover {
            color: ${color[p.color]};
            box-shadow: 0 0 0 35px ${color[p.color]} inset;
          }
        `
        case 'circle':
            return `
          width: 40px;
          height: 40px;
          border-radius: 50%;
          padding: 0 !important;
          position: relative;
          overflow: hidden;
          z-index: 1;
          border: none;
          background: transparent !important;
          box-shadow: 0 0 0 35px ${color[p.color]} inset;
          
          &:hover {
            color: ${color[p.color]};
            box-shadow: 0 0 0 1px ${color[p.color]} inset;
            border: none !important;
          }
        `
        default:
            return `
          border-radius: 2px;
        `
    }
}}
  
  ${(p) => {
    switch(p.size) {
        case 'lg':
            if(p.btnType === 'square' || p.btnType === 'circle') {
                return 'height: 48px; width: 48px;'
            } else {
                return 'padding: 12px 25px !important;font-size: 18px;'
            }
        case 'sm':
            if(p.btnType === 'square' || p.btnType === 'circle') {
                return 'height: 35px; width: 35px;'
            } else {
                return 'padding: 8px 25px !important;font-size: 12px;'
            }
        case 'xs':
            if(p.btnType === 'square' || p.btnType === 'circle' || p.btnType === 'outlineSquare' ) {
                return 'height: 30px; width: 30px;'
            } else {
                return 'padding: 4px 25px !important;font-size: 12px;'
            }
        case 'pagination':
            return 'min-width: 21px; padding: 7px 14px !important; font-size: 14px;'
        default:
            if(p.btnType === 'square' || p.btnType === 'circle') {
                return 'padding: 0 !important'
            } else {
                return 'padding: 10px 25px !important;font-size: 14px;'
            }
    }
}}
  
  &:disabled {
    background: #dcdcdc !important;
    border: solid 1px #dcdcdc !important;
    color: #212121;
    box-shadow: none;
    opacity: .65;
  }
  
  ${(p) => p.wide && css`
    width: 100%;
  `}
  
  ${(p) => p.shrink && css`
    flex-shrink: ${p.shrink};
  `}
  
  ${noTextSelection}
`

Button.propTypes = {
    color: PropTypes.string,
}

Button.defaultProps = {
    color: 'default',
}

export const GreenButton = styled(Button)`
    display: flex;
    padding: 0.75rem 2rem;
    color: rgb(255, 255, 255);
    background-color: #084;
    font-weight: 600;
    border-radius: 0.25rem;
    
    &:hover {
        background: #036635;
    }
`

export const BlueButton = styled(Button)`
    display: flex;
    padding: 0.75rem 2rem;
    color: #fff;
    font-size: 16px;
    background-color: #0044cc;
    font-kerning: auto;
    font-optical-sizing: auto;
    font-weight: 500;
    border-radius: 0.25rem;
    align-items: center;
    
    &:hover {
        background: #0033AA;
    }
    
`