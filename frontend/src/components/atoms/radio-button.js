import styled, {css} from "styled-components";
import {Container} from "./container";

export const RadioButton = styled(Container)`
  cursor: pointer;
  border-radius: 4px;
  transition: color, background 0.2s linear;
  font-weight: 600;
  border: 1px solid rgb(223, 225, 230);
  background-color: ${p => p.isDisabled ? '#f0f9f9' : '#fff'};
  color: ${p => p.isDisabled ? '#a6cece' : '#4D9898'};
  padding: ${p => p.padding ? p.padding : '0px'};
  font-weight: ${p => p.weight ? p.weight : 300};
  font-size: 17px;
  line-height: 24px;

  &:hover, &:active {
    border: none;
    background-color: ${p => p.isDisabled ? '#f0f9f9' : '#dcdcdc'};
    color: ${p => p.isDisabled ? '#a6cece' : '#212121'};
  }

  ${p => p.isSelected && css`
    border: none;
    background-color: ${p => p.isDisabled ? '#f0f9f9' : '#dcdcdc'};
    color: ${p => p.isDisabled ? '#a6cece' : '#212121'};
  `}
`
