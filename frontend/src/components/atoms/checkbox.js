import React, {PureComponent} from 'react'
import styled, {css} from 'styled-components'
import {Container} from "./container";
import {Text} from "./text";
import {color} from "../theme";

const Label = styled.label`
  display: inline-block;
  position: relative;
  padding-left: 16px;
  cursor: ${p => p.isDisabled ? 'defualt' : 'pointer'};
`

const StyledText = styled(Text)`
  color: ${p => p.isDisabled ? '#a6cece' : color['lead2']};
  font-weight: 600;
  font-size: 17px;
  line-height: 24px;
`

const CheckboxBlock = styled(Container)`
  min-height: 27px;
  margin-top: 0;
  margin-bottom: 0;
  
  & input {
    display: none;
  }
  
  &:last-child {
    padding-bottom: 0 !important;
  }
`

export const Checkbox = (props) => {
    const {
        isDisabled,
        label,
        name,
        width,
        type,
        value,
        colored = false,
        checkBoxType,
        isSelected
    } = props

    return (
        <CheckboxBlock align={'center'} padding={'0 0 7px'} width={width}>
            <input
                type={type}
                id={name + value}
                name={name + value}
                checked={isSelected}
                disabled={isDisabled}
            />
            <Icon
                type={isSelected ? 'checkbox_checked' : 'checkbox'}
                shrink={'0'}
                width={16}
                height={16}
                color={isDisabled ? '#dcdede' : isSelected ? '#269999' : '#999999'}
            />
            <Label
                htmlFor={value}
                isDisabled={isDisabled}
                isSelected={isSelected}
            >
                {label && <StyledText size={'14px'} isDisabled={isDisabled}>{label}</StyledText>}
            </Label>
        </CheckboxBlock>
    )
}



