import styled, {css} from "styled-components";
import {color} from "../theme";
import {Text} from './text'

export const RadioText = styled(Text)`
   font-size: ${p => p.size ? p.size : '13px'};
   font-weight: 500;

   ${p => p.isSelected && css`
       color: #212121;
   `}
`
