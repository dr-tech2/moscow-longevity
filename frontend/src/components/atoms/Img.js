import styled, {css} from 'styled-components'

export const Img = styled.img`
  max-height:100%;
  width: ${(p) => p.width};
  height: ${(p) => p.height};
  overflow: hidden;
  
  ${(p) => p.borderRadius && css`
     border-radius: ${p.borderRadius};  
  `}
 
  ${(p) => p.shrink && css`
     flex-shrink: ${p.shrink};
   `}
`