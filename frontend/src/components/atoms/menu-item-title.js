import React from "react";
import styled from "styled-components";
import {Text} from "./text";

export const MenuItemTitle = styled(Text)`
    font-size: 16px;
    line-height: 24px;
    font-weight: 600;
`