import React, {PureComponent} from 'react'
import styled, {css} from 'styled-components'
import {Container} from "./container";
import {Text} from "./text";

const Input = styled.input`
  opacity: 0;
`

const StyledImg = styled(Container)`
  display: inline-block;
  width: 78px;
  height: 78px;
  background-image: url("${(p) => p.png}");
  background-size: 64px 64px;
  background-repeat: no-repeat;
  background-position: center;
  
  ${(p) => (p.hovered || p.checked) && css`
    background-image: url("${(p) => p.svg}");
    background-size: 90px 90px;
  `}
`

const ImageContainer = styled(Container)`
  padding: 16px;
  border: 4px Solid #dfe1e6;
  cursor: pointer;
  transition: background 0.1s ease;
  -webkit-transition: background 0.1s ease;
  -ms-transition: background 0.1s ease;
  
  ${(p) => p.checked && css`
    border: 4px Solid #0044cc;
    background: radial-gradient(at top left, rgba(0, 68, 204, .2) 0%, #fff 28%, #fff 72%, rgba(0, 68, 204, .2) 100%);
  `}
`

const StyledContainer = styled(Container)`
    min-width: 320px;
`

export class CheckboxWithImage extends PureComponent {
    constructor(props) {
        super(props)

        this.state = {
            hovered: false,
        }
    }

    onClick = () => {
        // this.input.focus()
        !this.props.disabled && this.props.onClick()
    }

    render() {
        const {label, name, checked, disabled, mobile, description, setPopupParams} = this.props
        const content = description && <Container gap={16} flow={'column'}>
            <Text color={'#0044cc'} weight={600}>{label}</Text>
            <Text>{description}</Text>
        </Container>;

        return (
            <StyledContainer
                flow={'row'}
                align={'center'}
                margin={'22px 0 0 0'}
                onClick={this.onClick}
            >
                <ImageContainer
                    flow={'column'}
                    align={'center'}
                    onMouseEnter={(e) => {
                        this.setState({hovered: true})

                        const rect = e.currentTarget.getBoundingClientRect();
                        content && setPopupParams({
                            visible: true,
                            content: content,
                            position: {
                                left: rect.left + rect.width + 5,
                                top: rect.top - 10,
                            }
                        })
                    }}
                    onMouseLeave={(e) => {
                        this.setState({hovered: false})
                        setPopupParams({
                            visible: false,
                            content: null,
                            position: {}
                        })
                    }}
                    checked={checked}
                    gap={8}
                >
                    <StyledImg
                        png={'/img/' + label + '.png'}
                        svg={'/img/' + label + '.gif'}
                        hovered={this.state.hovered}
                        checked={checked}
                    />
                    <Container>
                        <Text align={'center'}>{label}</Text>
                    </Container>
                </ImageContainer>
                <Input type={'checkbox'} name={name} innerRef={(field) => {
                    this.input = field
                }} checked={checked} disabled={disabled}/>
            </StyledContainer>
        )
    }
}
