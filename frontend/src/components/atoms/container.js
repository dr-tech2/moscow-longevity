import styled, {css} from 'styled-components'
import PropTypes from 'prop-types'
import {color} from "../theme";

const marginDir = (p) => p.flow === 'column'
    ? 'top'
    : p.flow === 'row-reverse' ? 'right' : 'left'

export const Container = styled.div`
  display: ${(p) => p.inline ? 'inline-flex' : 'flex'};
  flex-direction: ${(p) => p.flow};
  flex-wrap: ${(p) => p.wrap};
  padding: ${(p) => p.padding};
  font-size: 0;
  
  ${(p) => p.margin && css`
    margin: ${p.margin};
  `}

  ${(p) => p.bgColor && css`
    background-color: ${p.bgColor};
  `}

  ${(p) => p.pointer && css`
    cursor: pointer;
  `}

  ${(p) => p.width && css`
    width: ${p.width};
  `}

  ${(p) => p.position && css`
    position: ${p.position};
  `}

  ${(p) => p.height && css`
    height: ${p.height};
  `}

  ${(p) => p.minHeight && css`
    min-height: ${p.minHeight};
  `}

  ${(p) => p.justify && css`
    justify-content: ${p.justify};
  `}

  ${(p) => p.align && css`
    align-items: ${p.align};
  `}

  ${(p) => p.borderRadius && css`
    border-radius: ${p.borderRadius};
  `}

  ${(p) => p.grow && css`
    flex-grow: ${p.grow};
  `}

  ${(p) => p.shrink && css`
    flex-shrink: ${p.shrink};
  `}


  ${(p) => p.gap && css`
    & > * + * {
      margin-${marginDir}: ${p.gap}px !important;
    }
  `}
`

Container.propTypes = {
    inline: PropTypes.bool,
    flow: PropTypes.oneOf(['row', 'column', 'row-reverse', 'column-reverse']),
    wrap: PropTypes.oneOf(['wrap', 'nowrap', 'wrap-reverse']),
    padding: PropTypes.string,
    bgColor: PropTypes.string,
    gap: PropTypes.number,
    justify: PropTypes.oneOf(['center', 'flex-start', 'flex-end', 'space-between', 'space-around']),
    align: PropTypes.oneOf(['stretch', 'flex-start', 'flex-end', 'center', 'baseline']),
    width: PropTypes.string,
    height: PropTypes.string,
    position: PropTypes.string,
    borderRadius: PropTypes.string,
    shrink: PropTypes.string,
    pointer: PropTypes.bool,
}

Container.defaultProps = {
    inline: false,
    flow: 'row',
    wrap: 'nowrap',
    padding: '0',
    gap: 0,
    justify: undefined,
    align: undefined,
    width: '100%',
    height: 'auto',
    shrink: 'initial',
    pointer: false
}
