import React from "react";
import styled from "styled-components";
import {InputWithMask} from "./input-with-mask";

export const StyledInputWithMask = styled(InputWithMask)`
  display: block;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: transparent;
  border: none;
  border: 1px Solid #dfe1e6;
  border-radius: 0.25rem;
  background-image: none;
  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  outline: none;
  width: ${(p) => p.width ? p.width : '100%'};
  padding: ${(p) => p.padding ? p.padding : '6px 12px'};
  margin: ${(p) => p.margin ? p.margin : '0'};

  &::-webkit-input-placeholder {
    color: #adc3d3;
  }
`
