import React from "react";
import styled, {css} from 'styled-components'
import {Container} from "./container";
import {Text} from "./text";

const StyledContainer = styled(Container)`
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.08);
    width: auto;
`

const StyledRadio = styled(Container)` 
    background: #fff;    
    border-top: 1px solid #c4c8d0;
    border-right: 1px solid #c4c8d0;
    border-bottom: 1px solid #c4c8d0;
    border-radius: 0 4px 4px 0;
      
    &.active {
        background: #084;
        border-top: 1px solid #c4c8d0;
        border-left: 1px solid #c4c8d0;
        border-bottom: 1px solid #c4c8d0;
        border-radius: 4px 0 0 4px;
    }
    
    input {
        display: none;
    }
`

const Label = styled(Text)`
    padding: 12px 40px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
`

export const Toggle = ({}) => {
    const states = ['Онлайн', 'Очные'];
    const name = 'toggle';
    const state = 0;

    return <StyledContainer>
        {states.map((item, i) => <StyledRadio key={'tgl_' + i} className={state === i ? 'active' : ''}>
            <input type={'radio'} name={name} value={i}/>
            <Label color={state === i ? '#fff' : '#0e0e0f'}>{item}</Label>
        </StyledRadio>)}
    </StyledContainer>
}