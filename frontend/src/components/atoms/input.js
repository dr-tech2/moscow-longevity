import styled, { css }            from 'styled-components'
import { color, formControl }            from '../theme'

export const Input = styled.input`
  display: block;
  width: ${(p) => p.width ? p.width : '100%'};
  padding: ${(p) => p.padding ? p.padding : '6px 12px'};
  margin: ${(p) => p.margin ? p.margin : '0'};
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: transparent;
  border: 1px Solid #dfe1e6;
  border-radius: 0.25rem;
  background-image: none;
  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  outline: none;

  &::-webkit-input-placeholder {
    color: #adc3d3;
  }

  ${formControl}

  ${(p) => p.height && css`
    height: ${(p) => p.height};
  `}

  ${(p) => p.cursor && css`
    cursor: ${(p) => p.cursor} !important;
  `}
  ${(p) => p.align && css`
    text-align: ${(p) => p.align};
  `}

  ${(p) => p.borderColor && css`
    border: 1px solid ${color[p.borderColor]};
  `}
`