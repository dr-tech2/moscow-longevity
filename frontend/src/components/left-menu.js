import React, {Component} from "react";
import styled from "styled-components";
import {Container} from "./atoms/container";
import {Format} from "./molecules/format";
import {Direction} from "./molecules/direction";
import {Days} from "./molecules/days";
import {Text} from "./atoms/text";
import {GreenButton} from "./atoms/button";

const StyledContainer = styled(Container)`
    width: 100%;
    max-width: 280px;
`

class LeftMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <StyledContainer flow={'column'}>
            <Format />
            <Direction />
            <Days />
            <Container margin={'40px 0 0 0'}>
                <GreenButton>Показать</GreenButton>
            </Container>
            <Container margin={'24px 0 0 0'}>
                <Text color={'#696c71'}>Найдено 854 группы</Text>
            </Container>
        </StyledContainer>
    }
}

export default LeftMenu;