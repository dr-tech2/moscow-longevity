import React from "react";
import {Provider} from "react-redux";
import {store, history} from "../store";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Full from "./Full";

function App() {
  return (
        <Provider store={store}>
            <Router history={history}>
                <Routes>
                    <Route path={'*'} element={<Full history={history} />}/>
                    <Route path={'/questions/*'} element={<Full history={history} />}/>
                    <Route path={'/list/*'} element={<Full history={history} />}/>
                </Routes>
            </Router>
        </Provider>
  );
}

export default App;
