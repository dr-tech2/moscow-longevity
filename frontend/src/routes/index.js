import React, {Component} from 'react'
import {connect} from 'react-redux'
import {push} from 'react-router-redux'
import {BaseTemplate} from "../components/templates/base";
import {MainComponent} from "../pages";
import {ContentLoader} from "../components/atoms/content-loader";

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
        }
    }

    async componentDidMount() {

    }

    componentDidUpdate(prevProps) {

    }

    render() {
        const {loading} = this.state

        return (
            <BaseTemplate>
                <MainComponent
                    page={this.props.location.loc.pathname}
                    loading={loading}
                />
            </BaseTemplate>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.main.isLoading,
    }
}

const mapDispatchToProps = {
    push
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
