export const chuckArray = (array, chunkSize = 10) => {
    const output = [];
    for (let i = 0; i < array.length; i += chunkSize) {
       output.push(array.slice(i, i + chunkSize))
    }
    return output
}
