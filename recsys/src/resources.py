import os

PROJECT_DIR = os.path.abspath(os.path.join(os.path.realpath(__file__), '..', '..'))
DATA_PATH = os.path.join(PROJECT_DIR, 'data')
MODELS_PATH = os.path.join(PROJECT_DIR, 'models')

PROD_MODEL_PATH = os.path.join(MODELS_PATH, '')
