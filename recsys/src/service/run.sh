#!/bin/bash

gunicorn src.service.endpoints:app \
    --timeout 1000 \
    --workers 1 \
    --worker-class gevent \
    --bind 0.0.0.0:8000
