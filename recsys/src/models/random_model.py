import os
import random
import pandas as pd

from src.resources import DATA_PATH
from src.models.model import Model


class RandomModel(Model):
    def __init__(self):
        Model.__init__(self)
        self.activities_df = pd.read_excel(os.path.join(DATA_PATH, 'raw', 'dict.xlsx'))
        self.target_col = 'id_level3'

    def predict_proba(self, user_id, activity_type=None):
        proba_dict = {i: random.random() for i in self.activities_df[self.target_col]}

        if activity_type in ['Для ума', 'Для души', 'Для тела']:
            proba_dict = {k: v for k, v in proba_dict.items() if k in list(self.activities_df[
                self.activities_df['Разметка: Для ума/ Для души / Для тела'] == activity_type
            ][self.target_col])}

        return proba_dict
