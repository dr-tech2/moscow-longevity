import os
import numpy as np
import pandas as pd
from surprise import Dataset, Reader, SVDpp

from src.resources import DATA_PATH
from src.models.model import Model

BEST_PARAMS = {
    'n_factors': 20,
    'n_epochs': 80,
    'lr_all': 0.002,
    'random_state': 42
}


class SVDppGroupsRecsys(Model):
    def __init__(self):
        Model.__init__(self)
        self.groups_df = pd.read_csv(os.path.join(DATA_PATH, 'raw', 'groups.csv'))
        self.target_col = 'уникальный номер'
        self.algo = SVDpp(**BEST_PARAMS)
        self._init_model()

    def _init_model(self):
        print('Groups recsys model loading...', flush=True)
        user_group_df = pd.read_csv(os.path.join(DATA_PATH, 'preprocessed', 'user_group.csv'))
        user_group_df['visits_log'] = user_group_df['visits'].map(lambda x: np.log(x + 1))
        reader = Reader(rating_scale=(1, 5))
        data = Dataset.load_from_df(user_group_df[['user_id', 'group_id', 'visits_log']], reader)
        self.algo.fit(data.build_full_trainset())

    def predict_proba(self, user_id):
        proba_dict = {i: self.algo.predict(user_id, i).est for i in self.groups_df[self.target_col]}

        return proba_dict
