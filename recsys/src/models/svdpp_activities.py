import os
import numpy as np
import pandas as pd
from surprise import Dataset, Reader, SVDpp

from src.resources import DATA_PATH
from src.models.model import Model

BEST_PARAMS = {'n_factors': 20,
               'n_epochs': 20,
               'lr_all': 0.002,
               'random_state': 42}


class SVDppActivitiesRecsys(Model):
    def __init__(self):
        Model.__init__(self)
        self.activities_df = pd.read_excel(os.path.join(DATA_PATH, 'raw', 'dict.xlsx'))
        self.target_col = 'id_level3'
        self.algo = SVDpp(**BEST_PARAMS)
        self._init_model()

    def _init_model(self):
        print('Activities recsys model loading...', flush=True)
        user_activity_df = pd.read_csv(os.path.join(DATA_PATH, 'preprocessed', 'user_activity.csv'))
        user_activity_df['visits_log'] = user_activity_df['visits'].map(lambda x: np.log(x + 1))
        reader = Reader(rating_scale=(1, 7))
        data = Dataset.load_from_df(user_activity_df[['user_id', 'activity_id', 'visits_log']], reader)
        self.algo.fit(data.build_full_trainset())

    def predict_proba(self, user_id, activity_type=None):
        proba_dict = {i: self.algo.predict(user_id, i).est for i in self.activities_df[self.target_col]}

        if activity_type in ['Для ума', 'Для души', 'Для тела']:
            proba_dict = {k: v for k, v in proba_dict.items() if k in list(self.activities_df[
                self.activities_df['Разметка: Для ума/ Для души / Для тела'] == activity_type
            ][self.target_col])}

        return proba_dict
